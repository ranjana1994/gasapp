-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2019 at 09:54 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gas-application`
--
CREATE DATABASE IF NOT EXISTS `gas-application` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gas-application`;

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'admin@gasapplication.com', '$2y$10$RP7SZT7NcGfaxhgEBWCO8OvKvJzFFb8rfwL.aeJ2SDi6FZ0MFHc0i', NULL, '2019-06-07 15:19:03', '2019-06-07 15:19:03', NULL),
(2, 'manager user', 'manager@gasapplication.com', '$2y$10$J2sDErph.Lrag7RqofJ9L.JoZGCGVqPmQDoLmp6vnmx5W847G9Aeu', NULL, '2019-06-07 19:21:17', '2019-06-07 19:21:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE `configurations` (
  `id` int(10) UNSIGNED NOT NULL,
  `tnc` text COLLATE utf8mb4_unicode_ci,
  `tnc_kr` text COLLATE utf8mb4_unicode_ci,
  `tnc_ar` text COLLATE utf8mb4_unicode_ci,
  `howto` text COLLATE utf8mb4_unicode_ci,
  `howto_kr` text COLLATE utf8mb4_unicode_ci,
  `howto_ar` text COLLATE utf8mb4_unicode_ci,
  `ads_ar` text COLLATE utf8mb4_unicode_ci,
  `ads_kr` text COLLATE utf8mb4_unicode_ci,
  `ads` text COLLATE utf8mb4_unicode_ci,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `configurations`
--

INSERT INTO `configurations` (`id`, `tnc`, `tnc_kr`, `tnc_ar`, `howto`, `howto_kr`, `howto_ar`, `ads_ar`, `ads_kr`, `ads`, `params`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'tnc english', 'tnckurdish', 'tncarbic', 'how to english', NULL, NULL, NULL, NULL, NULL, NULL, '0', '2019-06-07 08:19:09', '2019-06-07 08:19:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gas_notifications`
--

CREATE TABLE `gas_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `user_type` enum('supplier','customer') COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gas_notifications`
--

INSERT INTO `gas_notifications` (`id`, `user_id`, `title`, `message`, `user_type`, `notification_type`, `params`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Order Submitted', 'Order Submitted Successfully, waiting for your supplier to accept order', 'customer', 'order_pending', NULL, '0', '2019-07-04 03:12:22', '2019-07-04 03:12:22', NULL),
(2, 3, 'Order Received', 'Order Received, Kindly Accept the order.', 'supplier', 'order_pending', NULL, '0', '2019-07-04 03:12:23', '2019-07-04 03:12:23', NULL),
(3, 2, 'Order Submitted', 'Order Submitted Successfully, waiting for your supplier to accept order', 'customer', 'order_pending', NULL, '0', '2019-07-04 06:28:48', '2019-07-04 06:28:48', NULL),
(4, 2, 'Order Submitted', 'Order Submitted Successfully, waiting for your supplier to accept order', 'customer', 'order_pending', NULL, '0', '2019-07-04 06:29:33', '2019-07-04 06:29:33', NULL),
(5, 3, 'Order Received', 'Order Received, Kindly Accept the order.', 'supplier', 'order_pending', NULL, '0', '2019-07-04 06:29:34', '2019-07-04 06:29:34', NULL),
(6, 3, 'Order Completed ', 'Order Completed Successfully, Yeah !!! You have Completed the order.', 'supplier', 'order_completed', NULL, '0', '2019-07-05 00:18:40', '2019-07-05 00:18:40', NULL),
(7, 2, 'Order Submitted', 'Order Submitted Successfully, waiting for your supplier to accept order', 'customer', 'order_pending', NULL, '0', '2019-07-05 11:33:42', '2019-07-05 11:33:42', NULL),
(8, 3, 'Order Received', 'Order Received, Kindly Accept the order.', 'supplier', 'order_pending', NULL, '0', '2019-07-05 11:33:43', '2019-07-05 11:33:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `type` enum('governorate','city','section') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_kr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `parent_id`, `type`, `name`, `name_kr`, `name_ar`, `code`, `location`, `params`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'governorate', 'sulaymaniyah', 'sulaymaniyah', 'sulaymaniyah', '121', NULL, NULL, '1', '2019-06-07 08:20:34', '2019-06-07 15:20:40', NULL),
(2, 1, 'city', 'city center', 'city center', 'city center', '121', NULL, NULL, '1', '2019-06-07 08:20:34', '2019-06-07 15:20:40', NULL),
(3, 2, 'section', 'azadi', 'azadi', 'azadi', '101', NULL, NULL, '1', '2019-06-07 08:20:34', '2019-06-07 15:20:40', NULL),
(4, 2, 'section', 'Bakhtyrah', 'Bakhtyrah', 'Bakhtyrah', '201', NULL, NULL, '1', '2019-06-07 08:20:34', '2019-06-07 15:20:40', NULL),
(5, 2, 'section', 'Rzgary', 'Rzgary', 'Rzgary', '321', NULL, NULL, '1', '2019-06-07 08:20:34', '2019-06-07 15:20:40', NULL),
(6, 0, 'governorate', 'govenrante', 'kjhk', 'kjhkjh', '121', NULL, NULL, '1', '2019-06-07 19:22:52', '2019-07-06 05:59:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(10, '2019_05_16_100750_alter_user_table', 1),
(214, '2014_10_12_000000_create_users_table', 2),
(215, '2014_10_12_100000_create_password_resets_table', 2),
(216, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(217, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(218, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(219, '2016_06_01_000004_create_oauth_clients_table', 2),
(220, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(221, '2018_06_12_000000_create_admin_table', 2),
(222, '2018_12_21_134623_create_permission_tables', 2),
(223, '2019_05_16_142213_create_user_drivers_table', 2),
(224, '2019_05_17_101343_create_user_suppliers_table', 2),
(225, '2019_05_22_184902_create_services_table', 2),
(226, '2019_05_22_222530_create_configurations_table', 2),
(227, '2019_05_22_222533_create_locations_table', 2),
(228, '2019_05_24_121642_alter_configurations', 2),
(229, '2019_05_30_080827_create_user_addresses_table', 2),
(230, '2019_06_03_065210_create_orders_table', 2),
(231, '2019_06_12_110809_add_default_qty_to_services', 3),
(232, '2019_06_19_062027_create_order_children_table', 4),
(235, '2019_06_25_133421_create_user_supplier_favourites_table', 5),
(236, '2019_07_04_050821_create_gas_notifications_table', 5),
(237, '2019_07_10_083243_create_user_supplier_service_rates_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\AdminUser', 1),
(2, 'App\\User', 3),
(2, 'App\\User', 4),
(2, 'App\\User', 17),
(2, 'App\\User', 18),
(2, 'App\\User', 19),
(2, 'App\\User', 20),
(3, 'App\\User', 3),
(4, 'App\\User', 2),
(4, 'App\\User', 6),
(4, 'App\\User', 7),
(4, 'App\\User', 9),
(4, 'App\\User', 10),
(4, 'App\\User', 11),
(4, 'App\\User', 12),
(4, 'App\\User', 13),
(4, 'App\\User', 14),
(4, 'App\\User', 15),
(4, 'App\\User', 16),
(5, 'App\\User', 2),
(6, 'App\\User', 2),
(7, 'App\\User', 2),
(8, 'App\\User', 2),
(9, 'App\\User', 2),
(10, 'App\\User', 2),
(11, 'App\\User', 2),
(11, 'App\\User', 6),
(12, 'App\\User', 5),
(13, 'App\\AdminUser', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, 1, '111', 'DdoCiociSYHlrjkFrfwfAaNdJcFLjy676ff017zt', 'http://localhost/auth/callback', 0, 0, 0, '2019-06-07 15:20:11', '2019-06-07 15:20:11');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_customer_id` int(10) UNSIGNED NOT NULL,
  `user_customer_address_id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `is_child` int(11) NOT NULL DEFAULT '0',
  `service_quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_price` double(8,2) DEFAULT '0.00',
  `user_supplier_id` int(10) UNSIGNED NOT NULL,
  `request_status` enum('not accepted','canceled','processing','completed','rejected') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not accepted',
  `review_supplier` enum('1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `review_customer` enum('1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_customer_id`, `user_customer_address_id`, `service_id`, `is_child`, `service_quantity`, `service_price`, `user_supplier_id`, `request_status`, `review_supplier`, `review_customer`, `params`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(13, 4, 2, 1, 0, '4', 40.00, 6, 'completed', '4', '4', NULL, '1', '2019-06-28 02:30:00', '2019-07-10 10:56:01', NULL),
(14, 4, 1, 6, 0, '4', 40.00, 6, 'completed', '3', '4', NULL, '1', '2019-06-24 12:52:47', '2019-07-10 10:53:54', NULL),
(15, 2, 1, 4, 1, NULL, NULL, 6, 'processing', '2', '', NULL, '1', '2019-06-29 04:01:43', '2019-07-10 10:33:19', NULL),
(16, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 00:58:33', '2019-07-04 00:58:33', NULL),
(17, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 00:59:47', '2019-07-04 00:59:47', NULL),
(18, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 01:00:22', '2019-07-04 01:00:22', NULL),
(19, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 01:51:25', '2019-07-04 01:51:25', NULL),
(20, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:02:04', '2019-07-04 02:02:04', NULL),
(21, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:13:28', '2019-07-04 02:13:28', NULL),
(22, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:13:49', '2019-07-04 02:13:49', NULL),
(23, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:14:03', '2019-07-04 02:14:03', NULL),
(24, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:14:16', '2019-07-04 02:14:16', NULL),
(25, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:14:36', '2019-07-04 02:14:36', NULL),
(26, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:35:19', '2019-07-04 02:35:19', NULL),
(27, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:37:16', '2019-07-04 02:37:16', NULL),
(28, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:37:23', '2019-07-04 02:37:23', NULL),
(29, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:38:32', '2019-07-04 02:38:32', NULL),
(30, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:39:16', '2019-07-04 02:39:16', NULL),
(31, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:39:28', '2019-07-04 02:39:28', NULL),
(32, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:39:46', '2019-07-04 02:39:46', NULL),
(33, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:39:54', '2019-07-04 02:39:54', NULL),
(34, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:40:30', '2019-07-04 02:40:30', NULL),
(35, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:42:13', '2019-07-04 02:42:13', NULL),
(36, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:42:33', '2019-07-04 02:42:33', NULL),
(37, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:45:47', '2019-07-04 02:45:47', NULL),
(38, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 02:50:09', '2019-07-04 02:50:09', NULL),
(39, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 03:01:20', '2019-07-04 03:01:20', NULL),
(40, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 03:12:22', '2019-07-04 03:12:22', NULL),
(41, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-04 06:28:48', '2019-07-04 06:28:48', NULL),
(42, 2, 1, 4, 1, NULL, NULL, 6, 'completed', '3', '', NULL, '1', '2019-07-04 06:29:33', '2019-07-10 10:53:58', NULL),
(43, 2, 1, 4, 1, NULL, NULL, 3, 'not accepted', '1', '', NULL, '1', '2019-07-05 11:33:42', '2019-07-05 11:33:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_children`
--

CREATE TABLE `order_children` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `service_quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_price` double(8,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_children`
--

INSERT INTO `order_children` (`id`, `order_id`, `service_id`, `service_quantity`, `service_price`, `created_at`, `updated_at`) VALUES
(13, 15, 9, '4', 0.00, '2019-06-29 04:01:43', '2019-06-29 04:01:43'),
(14, 15, 10, '2', 0.00, '2019-06-29 04:01:43', '2019-06-29 04:01:43'),
(15, 16, 9, '4', 0.00, '2019-07-04 00:58:33', '2019-07-04 00:58:33'),
(16, 16, 10, '2', 0.00, '2019-07-04 00:58:33', '2019-07-04 00:58:33'),
(17, 17, 9, '4', 0.00, '2019-07-04 00:59:47', '2019-07-04 00:59:47'),
(18, 17, 10, '2', 0.00, '2019-07-04 00:59:47', '2019-07-04 00:59:47'),
(19, 18, 9, '4', 0.00, '2019-07-04 01:00:22', '2019-07-04 01:00:22'),
(20, 18, 10, '2', 0.00, '2019-07-04 01:00:22', '2019-07-04 01:00:22'),
(21, 19, 9, '4', 0.00, '2019-07-04 01:51:25', '2019-07-04 01:51:25'),
(22, 19, 10, '2', 0.00, '2019-07-04 01:51:25', '2019-07-04 01:51:25'),
(23, 20, 9, '4', 0.00, '2019-07-04 02:02:04', '2019-07-04 02:02:04'),
(24, 20, 10, '2', 0.00, '2019-07-04 02:02:04', '2019-07-04 02:02:04'),
(25, 21, 9, '4', 0.00, '2019-07-04 02:13:28', '2019-07-04 02:13:28'),
(26, 21, 10, '2', 0.00, '2019-07-04 02:13:28', '2019-07-04 02:13:28'),
(27, 22, 9, '4', 0.00, '2019-07-04 02:13:49', '2019-07-04 02:13:49'),
(28, 22, 10, '2', 0.00, '2019-07-04 02:13:49', '2019-07-04 02:13:49'),
(29, 23, 9, '4', 0.00, '2019-07-04 02:14:03', '2019-07-04 02:14:03'),
(30, 23, 10, '2', 0.00, '2019-07-04 02:14:03', '2019-07-04 02:14:03'),
(31, 24, 9, '4', 0.00, '2019-07-04 02:14:16', '2019-07-04 02:14:16'),
(32, 24, 10, '2', 0.00, '2019-07-04 02:14:16', '2019-07-04 02:14:16'),
(33, 25, 9, '4', 0.00, '2019-07-04 02:14:36', '2019-07-04 02:14:36'),
(34, 25, 10, '2', 0.00, '2019-07-04 02:14:36', '2019-07-04 02:14:36'),
(35, 26, 9, '4', 0.00, '2019-07-04 02:35:19', '2019-07-04 02:35:19'),
(36, 26, 10, '2', 0.00, '2019-07-04 02:35:19', '2019-07-04 02:35:19'),
(37, 27, 9, '4', 0.00, '2019-07-04 02:37:16', '2019-07-04 02:37:16'),
(38, 27, 10, '2', 0.00, '2019-07-04 02:37:16', '2019-07-04 02:37:16'),
(39, 28, 9, '4', 0.00, '2019-07-04 02:37:23', '2019-07-04 02:37:23'),
(40, 28, 10, '2', 0.00, '2019-07-04 02:37:23', '2019-07-04 02:37:23'),
(41, 29, 9, '4', 0.00, '2019-07-04 02:38:32', '2019-07-04 02:38:32'),
(42, 29, 10, '2', 0.00, '2019-07-04 02:38:32', '2019-07-04 02:38:32'),
(43, 30, 9, '4', 0.00, '2019-07-04 02:39:16', '2019-07-04 02:39:16'),
(44, 30, 10, '2', 0.00, '2019-07-04 02:39:16', '2019-07-04 02:39:16'),
(45, 31, 9, '4', 0.00, '2019-07-04 02:39:28', '2019-07-04 02:39:28'),
(46, 31, 10, '2', 0.00, '2019-07-04 02:39:28', '2019-07-04 02:39:28'),
(47, 32, 9, '4', 0.00, '2019-07-04 02:39:46', '2019-07-04 02:39:46'),
(48, 32, 10, '2', 0.00, '2019-07-04 02:39:46', '2019-07-04 02:39:46'),
(49, 33, 9, '4', 0.00, '2019-07-04 02:39:54', '2019-07-04 02:39:54'),
(50, 33, 10, '2', 0.00, '2019-07-04 02:39:54', '2019-07-04 02:39:54'),
(51, 34, 9, '4', 0.00, '2019-07-04 02:40:30', '2019-07-04 02:40:30'),
(52, 34, 10, '2', 0.00, '2019-07-04 02:40:30', '2019-07-04 02:40:30'),
(53, 35, 9, '4', 0.00, '2019-07-04 02:42:13', '2019-07-04 02:42:13'),
(54, 35, 10, '2', 0.00, '2019-07-04 02:42:13', '2019-07-04 02:42:13'),
(55, 36, 9, '4', 0.00, '2019-07-04 02:42:33', '2019-07-04 02:42:33'),
(56, 36, 10, '2', 0.00, '2019-07-04 02:42:33', '2019-07-04 02:42:33'),
(57, 37, 9, '4', 0.00, '2019-07-04 02:45:47', '2019-07-04 02:45:47'),
(58, 37, 10, '2', 0.00, '2019-07-04 02:45:47', '2019-07-04 02:45:47'),
(59, 38, 9, '4', 0.00, '2019-07-04 02:50:09', '2019-07-04 02:50:09'),
(60, 38, 10, '2', 0.00, '2019-07-04 02:50:09', '2019-07-04 02:50:09'),
(61, 39, 9, '4', 0.00, '2019-07-04 03:01:20', '2019-07-04 03:01:20'),
(62, 39, 10, '2', 0.00, '2019-07-04 03:01:20', '2019-07-04 03:01:20'),
(63, 40, 9, '4', 0.00, '2019-07-04 03:12:22', '2019-07-04 03:12:22'),
(64, 40, 10, '2', 0.00, '2019-07-04 03:12:22', '2019-07-04 03:12:22'),
(65, 41, 9, '4', 0.00, '2019-07-04 06:28:48', '2019-07-04 06:28:48'),
(66, 41, 10, '2', 0.00, '2019-07-04 06:28:48', '2019-07-04 06:28:48'),
(67, 42, 9, '4', 0.00, '2019-07-04 06:29:33', '2019-07-04 06:29:33'),
(68, 42, 10, '2', 0.00, '2019-07-04 06:29:33', '2019-07-04 06:29:33'),
(69, 43, 9, '4', 0.00, '2019-07-05 11:33:42', '2019-07-05 11:33:42'),
(70, 43, 10, '2', 0.00, '2019-07-05 11:33:42', '2019-07-05 11:33:42');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'api/user', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(2, 'api/configuration/{type}', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(3, 'api/customer/login', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(4, 'api/customer/verify-login', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(5, 'api/customer/registeration', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(6, 'api/customer/location/store', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(7, 'api/customer/location/{id}', 'api', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(8, 'api/customer/services', 'api', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(9, 'api/customer/service/{governateid}', 'api', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(10, 'api/customer/order/store', 'api', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(11, 'api/customer/order/{id}', 'api', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(12, 'api/location/{type}', 'api', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(13, 'api/location/{type}/{parent_id}', 'api', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(14, '/', 'api', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(15, 'admin', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(16, 'admin/delete-all/{model}', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(17, 'admin/login', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(18, 'admin/password-reset', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(19, 'admin/password/reset/{token}', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(20, 'admin/password/reset', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(21, 'admin/dashboard', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(22, 'admin/adminuser', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(23, 'admin/adminuser/create', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(24, 'admin/adminuser/{adminuser}', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(25, 'admin/adminuser/{adminuser}/edit', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(26, 'admin/users', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(27, 'admin/users/create', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(28, 'admin/users/{user}', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(29, 'admin/users/{user}/edit', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(30, 'admin/roles', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(31, 'admin/roles/create', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(32, 'admin/roles/{role}', 'web-admin', '2019-06-07 15:18:53', '2019-06-07 15:18:53'),
(33, 'admin/roles/{role}/edit', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(34, 'admin/role-fetch', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(35, 'admin/customers', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(36, 'admin/customers/create', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(37, 'admin/customers/{customer}', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(38, 'admin/customers/{customer}/edit', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(39, 'admin/suppliers', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(40, 'admin/suppliers/create', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(41, 'admin/suppliers/{supplier}', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(42, 'admin/suppliers/{supplier}/edit', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(43, 'admin/drivers', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(44, 'admin/drivers/create', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(45, 'admin/drivers/{driver}', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(46, 'admin/drivers/{driver}/edit', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(47, 'admin/services', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(48, 'admin/services/create', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(49, 'admin/services/{service}', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(50, 'admin/services/{service}/edit', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(51, 'admin/governorate', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(52, 'admin/governorate/create', 'web-admin', '2019-06-07 15:18:54', '2019-06-07 15:18:54'),
(53, 'admin/governorate/{governorate}', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(54, 'admin/governorate/{governorate}/edit', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(55, 'admin/city', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(56, 'admin/city/create', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(57, 'admin/city/{city}', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(58, 'admin/city/{city}/edit', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(59, 'admin/section', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(60, 'admin/section/create', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(61, 'admin/section/{section}', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(62, 'admin/section/{section}/edit', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(63, 'admin/admin-roles', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(64, 'admin/admin-roles/create', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(65, 'admin/admin-roles/{admin_role}', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(66, 'admin/admin-roles/{admin_role}/edit', 'web-admin', '2019-06-07 15:18:55', '2019-06-07 15:18:55'),
(67, 'admin/admin-role-fetch', 'web-admin', '2019-06-07 15:18:56', '2019-06-07 15:18:56'),
(68, 'admin/import-csv-excel', 'web-admin', '2019-06-07 15:18:56', '2019-06-07 15:18:56'),
(69, 'admin/import-customer-csv-excel', 'web-admin', '2019-06-07 15:18:56', '2019-06-07 15:18:56'),
(70, 'admin/import-supplier-csv-excel', 'web-admin', '2019-06-07 15:18:56', '2019-06-07 15:18:56'),
(71, 'admin/configuration', 'web-admin', '2019-06-07 15:18:56', '2019-06-07 15:18:56'),
(72, 'admin/location/governorate/changestate/{id}', 'web-admin', '2019-06-07 15:18:56', '2019-06-07 15:18:56'),
(73, 'admin/delete-state', 'web-admin', '2019-06-07 15:18:56', '2019-06-07 15:18:56'),
(74, 'admin/services/changestate/{id}', 'web-admin', '2019-06-07 15:18:56', '2019-06-07 15:18:56'),
(75, 'admin/user/changestate/{id}', 'web-admin', '2019-06-07 15:18:56', '2019-06-07 15:18:56'),
(76, 'admin/notification/{type}/{target}', 'web-admin', '2019-06-07 15:18:57', '2019-06-07 15:18:57'),
(77, 'logout', 'api', '2019-06-07 15:18:57', '2019-06-07 15:18:57'),
(78, 'login', 'api', '2019-06-07 15:18:57', '2019-06-07 15:18:57'),
(79, 'dashboard', 'api', '2019-06-07 15:18:57', '2019-06-07 15:18:57'),
(80, 'services', 'api', '2019-06-07 15:18:57', '2019-06-07 15:18:57'),
(81, 'api/customer/suppliers/{serviceid}/{governateid}', 'api', '2019-06-07 16:38:44', '2019-06-07 16:38:44'),
(82, 'profile', 'api', '2019-06-07 16:38:44', '2019-06-07 16:38:44'),
(83, 'order', 'api', '2019-06-07 16:38:44', '2019-06-07 16:38:44'),
(84, 'save', 'api', '2019-06-07 16:38:44', '2019-06-07 16:38:44'),
(85, 'api/customer/suppliers/{servicename}/{governateid}', 'api', '2019-06-07 17:00:14', '2019-06-07 17:00:14'),
(86, 'api/customer/suppliers/{servicename}/{governateid}/{mobilenumber}', 'api', '2019-06-07 17:17:07', '2019-06-07 17:17:07'),
(87, 'admin/customers/search', 'web-admin', '2019-06-07 19:52:32', '2019-06-07 19:52:32'),
(88, 'admin/customers-search', 'web-admin', '2019-06-07 19:55:05', '2019-06-07 19:55:05'),
(89, 'admin/suppliers-search', 'web-admin', '2019-06-07 20:01:40', '2019-06-07 20:01:40'),
(90, 'api/customer/resend-otp', 'api', '2019-06-19 02:46:44', '2019-06-19 02:46:44'),
(91, 'api/customer/location/update/{id}', 'api', '2019-06-19 02:46:44', '2019-06-19 02:46:44'),
(92, 'lang/{locale}', 'api', '2019-06-19 02:46:44', '2019-06-19 02:46:44'),
(93, 'order-gas', 'api', '2019-06-19 02:46:44', '2019-06-19 02:46:44'),
(94, 'number', 'api', '2019-06-19 02:46:44', '2019-06-19 02:46:44'),
(95, 'order/{id}', 'api', '2019-06-19 02:46:44', '2019-06-19 02:46:44'),
(96, 'api/supplier/login/{service_id}', 'api', '2019-06-24 07:35:08', '2019-06-24 07:35:08'),
(97, 'api/supplier/verify-login', 'api', '2019-06-24 07:35:08', '2019-06-24 07:35:08'),
(98, 'api/supplier/order/{supplierId}', 'api', '2019-06-24 07:35:08', '2019-06-24 07:35:08'),
(99, 'api/supplier/order/update/{supplierId}/{orderid}', 'api', '2019-06-24 10:41:40', '2019-06-24 10:41:40'),
(100, 'api/supplier/services', 'api', '2019-06-25 02:09:33', '2019-06-25 02:09:33'),
(101, 'api/supplier/{id}', 'api', '2019-06-25 05:14:44', '2019-06-25 05:14:44'),
(102, 'api/supplier/user/{id}', 'api', '2019-06-25 07:39:39', '2019-06-25 07:39:39'),
(103, 'api/supplier/order/{supplierId}/{serviceid}', 'api', '2019-06-27 22:14:45', '2019-06-27 22:14:45'),
(104, 'api/supplier/driver/{supplierid}', 'api', '2019-06-27 22:14:45', '2019-06-27 22:14:45'),
(105, 'api/supplier/driver/update/{driverid}', 'api', '2019-06-27 22:14:45', '2019-06-27 22:14:45'),
(106, 'api/supplier/driver/{driverid}', 'api', '2019-06-27 22:14:45', '2019-06-27 22:14:45'),
(107, 'api/supplier/favourite/{supplier_id}/{customer_id}', 'api', '2019-06-27 22:14:45', '2019-06-27 22:14:45'),
(108, 'api/supplier/favourite/{supplier_id}', 'api', '2019-06-27 22:14:45', '2019-06-27 22:14:45'),
(109, 'api/customer/order/{id}/{serviceid}', 'api', '2019-06-27 22:20:26', '2019-06-27 22:20:26'),
(110, 'api/customer/order/update/{supplierId}/{orderid}', 'api', '2019-06-27 23:02:56', '2019-06-27 23:02:56'),
(111, 'api/auth/change-password', 'api', '2019-06-28 00:04:56', '2019-06-28 00:04:56'),
(112, 'api/without-auth/change-password', 'api', '2019-06-28 01:07:08', '2019-06-28 01:07:08'),
(113, 'api/customer/order/{customerid}/{orderid}', 'api', '2019-06-28 01:58:35', '2019-06-28 01:58:35'),
(114, 'api/notifications/{user_type}', 'api', '2019-07-04 03:00:00', '2019-07-04 03:00:00'),
(115, 'api/notifications/{user_type}/{user_id}', 'api', '2019-07-04 03:14:29', '2019-07-04 03:14:29'),
(116, 'api/customer/update/{id}', 'api', '2019-07-05 00:58:10', '2019-07-05 00:58:10'),
(117, 'api/customer/update-number/{id}', 'api', '2019-07-05 01:13:08', '2019-07-05 01:13:08'),
(118, 'api/customer/verify-number/{id}', 'api', '2019-07-05 02:37:38', '2019-07-05 02:37:38'),
(119, 'api/supplier/registeration', 'api', '2019-07-06 04:55:12', '2019-07-06 04:55:12'),
(120, 'api/supplier/location/store', 'api', '2019-07-06 07:43:31', '2019-07-06 07:43:31'),
(121, 'api/supplier/location/update/{id}', 'api', '2019-07-06 07:43:31', '2019-07-06 07:43:31'),
(122, 'api/supplier/location/{id}', 'api', '2019-07-06 07:43:31', '2019-07-06 07:43:31'),
(123, 'api/supplier/location/update', 'api', '2019-07-10 02:33:28', '2019-07-10 02:33:28'),
(124, 'api/supplier/rate-card', 'api', '2019-07-10 03:36:35', '2019-07-10 03:36:35'),
(125, 'api/supplier/get-rate-card', 'api', '2019-07-11 01:38:58', '2019-07-11 01:38:58');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'super admin', 'web-admin', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(2, 'home customer', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(3, 'small business customer', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(4, 'gas supplier', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(5, 'lpg supplier', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(6, 'oil supplier', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(7, 'bottled water supplier', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(8, 'tanker water supplier', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(9, 'barrel fuel supplier', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(10, 'per liter fuel supplier', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(11, 'diesel supplier', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(12, 'driver', 'api', '2019-06-07 15:18:52', '2019-06-07 15:18:52'),
(13, 'manager', 'web-admin', '2019-06-07 19:19:02', '2019-06-07 19:19:02');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(5, 2),
(5, 3),
(5, 4),
(5, 5),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(7, 2),
(7, 3),
(7, 4),
(7, 5),
(8, 2),
(8, 3),
(8, 4),
(8, 5),
(9, 2),
(9, 3),
(9, 4),
(9, 5),
(10, 2),
(10, 3),
(10, 4),
(10, 5),
(11, 2),
(11, 3),
(11, 4),
(11, 5),
(12, 2),
(12, 3),
(12, 4),
(12, 5),
(13, 2),
(13, 3),
(13, 4),
(13, 5),
(14, 2),
(14, 3),
(14, 4),
(14, 5),
(15, 1),
(15, 13),
(16, 1),
(17, 1),
(17, 13),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(21, 13),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(38, 13),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(51, 13),
(52, 1),
(52, 13),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 2),
(77, 3),
(77, 4),
(77, 5),
(78, 2),
(78, 3),
(78, 4),
(78, 5),
(79, 2),
(79, 3),
(79, 4),
(79, 5),
(80, 2),
(80, 3),
(80, 4),
(80, 5),
(81, 2),
(81, 3),
(81, 4),
(81, 5),
(82, 2),
(82, 3),
(82, 4),
(82, 5),
(83, 2),
(83, 3),
(83, 4),
(83, 5),
(84, 2),
(84, 3),
(84, 4),
(84, 5),
(85, 2),
(85, 3),
(85, 4),
(85, 5),
(86, 2),
(86, 3),
(86, 4),
(86, 5),
(87, 1),
(88, 1),
(88, 13),
(89, 1),
(89, 13),
(90, 2),
(90, 3),
(90, 4),
(90, 5),
(91, 2),
(91, 3),
(91, 4),
(91, 5),
(92, 2),
(92, 3),
(92, 4),
(92, 5),
(93, 2),
(93, 3),
(93, 4),
(93, 5),
(94, 2),
(94, 3),
(94, 4),
(94, 5),
(95, 2),
(95, 3),
(95, 4),
(95, 5),
(96, 2),
(96, 3),
(96, 4),
(96, 5),
(97, 2),
(97, 3),
(97, 4),
(97, 5),
(98, 2),
(98, 3),
(98, 4),
(98, 5),
(99, 2),
(99, 3),
(99, 4),
(100, 2),
(100, 3),
(100, 4),
(101, 2),
(101, 3),
(101, 4),
(102, 2),
(102, 3),
(102, 4),
(103, 2),
(103, 3),
(103, 4),
(104, 2),
(104, 3),
(104, 4),
(105, 2),
(105, 3),
(105, 4),
(106, 2),
(106, 3),
(106, 4),
(107, 2),
(107, 3),
(107, 4),
(108, 2),
(108, 3),
(108, 4),
(109, 2),
(109, 3),
(109, 4),
(110, 2),
(110, 3),
(110, 4),
(111, 2),
(111, 3),
(111, 4),
(112, 2),
(112, 3),
(112, 4),
(113, 2),
(113, 3),
(113, 4),
(114, 2),
(114, 3),
(114, 4),
(115, 2),
(115, 3),
(115, 4),
(116, 2),
(116, 3),
(116, 4),
(117, 2),
(117, 3),
(117, 4),
(118, 3),
(118, 4),
(119, 4),
(120, 4),
(121, 4),
(122, 4),
(123, 4),
(124, 4),
(125, 4);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_kr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_kr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` text COLLATE utf8mb4_unicode_ci,
  `unit` enum('liter','quantity','set','barrel') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `default_qty` int(11) DEFAULT '0',
  `role_id` int(11) NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `image`, `name`, `image_kr`, `name_kr`, `image_ar`, `name_ar`, `location`, `unit`, `parent_id`, `default_qty`, `role_id`, `params`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1560840772.png', 'order gas', '1560840772.png', 'gazê', '1560840772.png', 'طلب الغاز', '[\"1\",\"6\"]', 'quantity', 0, 1, 4, NULL, '1', '2019-06-07 08:19:14', '2019-06-18 01:22:52', NULL),
(2, '1560840754.png', 'order fuel', '1560840754.png', 'firotin', '1560840754.png', 'طلب الوقود', '[\"1\",\"6\"]', 'liter', 0, 5, 10, NULL, '1', '2019-06-07 08:19:14', '2019-06-18 01:22:34', NULL),
(3, '1560840715.png', 'order lpg', '1560840715.png', 'lpg', '1560840715.png', 'أجل غاز البترول المسال', '[\"1\",\"6\"]', 'liter', 0, 1, 5, NULL, '1', '2019-06-07 08:19:14', '2019-06-18 01:21:55', NULL),
(4, '1560840805.png', 'order bottled', '1560840805.png', 'şîrîn', '1560840805.png', 'المعبأة في زجاجات ترتيب', '[\"1\",\"6\"]', 'liter', 0, 5, 7, NULL, '1', '2019-06-07 08:19:14', '2019-06-18 01:23:25', NULL),
(5, '1560840669.png', 'order diesel', '1560840669.png', 'birêvebirinê', '1560840669.png', 'طلب الديزل', '[\"1\",\"6\"]', 'liter', 0, 0, 11, NULL, '1', '2019-06-07 08:19:14', '2019-06-18 01:21:09', NULL),
(6, '1560840587.png', 'order fuel barel', '1560840587.png', 'birêkirina bermîlê', '1560840587.png', 'طلب باريل الوقود', '[\"1\",\"6\"]', 'liter', 0, 15, 9, NULL, '1', '2019-06-07 08:19:14', '2019-06-18 01:19:47', NULL),
(7, '1560840634.png', 'order oil', '1560840634.png', 'petrolê', '1560840634.png', 'طلب النفط', '[\"1\",\"6\"]', 'liter', 0, 0, 6, NULL, '1', '2019-06-07 08:19:14', '2019-06-18 01:20:34', NULL),
(8, '1560841049.png', 'order water', '1560841049.png', 'avê', '1560841049.png', 'طلب الماء', '[\"1\",\"6\"]', 'liter', 0, 0, 7, NULL, '1', '2019-06-07 08:19:14', '2019-06-18 01:27:29', NULL),
(9, '1560840495.png', 'Plastic Bowl', '1560840495.png', 'Plastic Bowl', '1560840495.png', 'Plastic Bowl', '[\"1\"]', 'set', 4, 10, 7, NULL, '1', '2019-06-10 17:37:05', '2019-06-18 01:18:15', NULL),
(10, '1560840527.png', 'size: 300 ml', '1560840527.png', 'size: 300 ml', '1560840527.png', 'size: 300 ml', '[\"1\",\"6\"]', 'quantity', 4, 5, 7, NULL, '1', '2019-06-10 17:44:23', '2019-06-18 01:18:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number_tmp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `otp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `device_token` text COLLATE utf8mb4_unicode_ci,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `email`, `mobile_number`, `mobile_number_tmp`, `otp`, `password`, `remember_token`, `status`, `device_token`, `params`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'test', 'supplier', '1', '752988376311', '0', '2415', '$2y$10$XnaQxMepbF8FXrlunL/0Ce9zAaRnDwhkayIkg2i7/0/eNeC1124Ga', NULL, '0', 'cBRKFIufMiw:APA91bF9o7sQB1NY35QOO72Z38SNKOnVAEFYS9wVvNQoGsA4059yYIH_zzUNXTMHKVn1VOp0I1uyopr1KKAYKAPFtaQmqvOO7gf9U6iuoJeyt5Rv2tI9zvdqIhD7vnRmWU7OCkmWzy9Q', NULL, '1', '2019-06-07 17:07:28', '2019-06-25 07:41:11', NULL),
(3, 'rams', 'shams', NULL, '9646848501', '0', '0', '$2y$10$wwJNHYagXQA4R5mLJdjbr.c40ROJuT720L9MHjTSfAeJ0azhB0h6W', NULL, '0', 'cBRKFIufMiw:APA91bF9o7sQB1NY35QOO72Z38SNKOnVAEFYS9wVvNQoGsA4059yYIH_zzUNXTMHKVn1VOp0I1uyopr1KKAYKAPFtaQmqvOO7gf9U6iuoJeyt5Rv2tI9zvdqIhD7vnRmWU7OCkmWzy9Q', NULL, '1', '2019-06-11 21:59:21', '2019-07-05 02:40:22', NULL),
(4, 'ram', 'sham', NULL, '07728447522', '0', '5971', '$2y$10$y97ag.FqpnILYnLfbPjY4.6XkWeAqe2TedmGsXDoxCtJw5Fxe4SgG', NULL, '1', 'cBRKFIufMiw:APA91bF9o7sQB1NY35QOO72Z38SNKOnVAEFYS9wVvNQoGsA4059yYIH_zzUNXTMHKVn1VOp0I1uyopr1KKAYKAPFtaQmqvOO7gf9U6iuoJeyt5Rv2tI9zvdqIhD7vnRmWU7OCkmWzy9Q', NULL, '1', '2019-06-11 22:00:59', '2019-06-11 22:01:00', NULL),
(5, 'test', NULL, NULL, '9875461230', '0', '3368', '$2y$10$ar7gWqX6JxB.JtvgTLTiE.gDJdKTprHhq/8NkoiOqVx6qoN6mXt0q', NULL, '1', 'cBRKFIufMiw:APA91bF9o7sQB1NY35QOO72Z38SNKOnVAEFYS9wVvNQoGsA4059yYIH_zzUNXTMHKVn1VOp0I1uyopr1KKAYKAPFtaQmqvOO7gf9U6iuoJeyt5Rv2tI9zvdqIhD7vnRmWU7OCkmWzy9Q', NULL, '0', '2019-06-24 08:50:33', '2019-07-08 03:49:33', NULL),
(6, 'test', 'test', NULL, '879654213', '0', NULL, '$2y$10$eF3tJKSfuHnbBMivo6MqP.wi4ur5b30VH64T.YkPGFyHxD/n4rnA.', NULL, '1', 'cBRKFIufMiw:APA91bF9o7sQB1NY35QOO72Z38SNKOnVAEFYS9wVvNQoGsA4059yYIH_zzUNXTMHKVn1VOp0I1uyopr1KKAYKAPFtaQmqvOO7gf9U6iuoJeyt5Rv2tI9zvdqIhD7vnRmWU7OCkmWzy9Q', NULL, '1', '2019-06-24 08:51:13', '2019-07-10 04:53:20', NULL),
(7, 'ram', 'sham', NULL, '7529883763', '0', '4639', '$2y$10$fII79Wfyls5gu8lR9MGXNu7yq0ZTWDeFCyfyRpJrpQTsmcGVllfc.', NULL, '1', NULL, NULL, '1', '2019-07-06 04:56:14', '2019-07-10 04:53:19', NULL),
(9, 'ram', 'sham', NULL, '7529883761', '0', '7962', '$2y$10$JdOkYsj8cfj3HTpqcXqx1Oyr8obeSJm3oKJNrhPCUEXx1Zdl2aHV2', NULL, '1', NULL, NULL, '0', '2019-07-06 04:57:08', '2019-07-08 03:56:35', NULL),
(10, 'ram', 'sham', NULL, '75298837631', '0', '8396', '$2y$10$bWqU2j5QGFXYLK1AidHVmeCcK5M9DQELh8wwqV9O/6QnEUuhKPFry', NULL, '1', NULL, NULL, '1', '2019-07-08 03:57:07', '2019-07-10 04:53:17', NULL),
(11, 'ram', 'sham', NULL, '75298837611', '0', '5083', '$2y$10$uzxQW3nNs2y6oZBMJ4WPcOAM85aOwMrR0vHRr0y2jztKda1TT5eJG', NULL, '1', NULL, NULL, '1', '2019-07-08 04:23:20', '2019-07-10 04:53:15', NULL),
(12, 'ram', 'sham', NULL, '7529883762', '0', '0', '$2y$10$wf9L9dF5i2TvbdSSNbyQJeqE1hTLk5P8lXkcayvVyecO./eccitdy', NULL, '1', 'dsdsffdssdfsdf', NULL, '0', '2019-07-08 04:24:01', '2019-07-08 04:27:37', NULL),
(13, 'ram', 'sham', NULL, '7529883722', '0', '6932', '$2y$10$JzWAb.M/ChuwuFqRWrGlXOSnMmgU25CDqveSP2noiyFrb2yRP25nS', NULL, '1', NULL, NULL, '1', '2019-07-08 04:34:31', '2019-07-10 04:53:13', NULL),
(14, 'ram', 'sham', NULL, '75298837221', '0', '8343', '$2y$10$zlx/82HLtUPJsV9qibbgJ.e8T3ytWy1ILrDJnDM.aotns9QWin3kW', NULL, '1', NULL, NULL, '1', '2019-07-08 08:50:24', '2019-07-10 04:53:11', NULL),
(15, 'ram', 'sham', NULL, '75298837254', '0', NULL, '$2y$10$htidz9RynKRALYd7NZdbieI/durg8rtnrFxYQ.HE4Raxbf9eZo3OO', NULL, '1', NULL, NULL, '1', '2019-07-09 00:23:32', '2019-07-10 04:53:10', NULL),
(16, 'ram', 'sham', NULL, '752988337254', '0', '9765', '$2y$10$vL/9QRW5BXd.yOAtqdpMBOEEP7WopZWr2rJkYMfnhUVsMzOeSug6q', NULL, '1', NULL, NULL, '1', '2019-07-09 00:25:50', '2019-07-10 04:53:08', NULL),
(17, 'ram', 'sham', NULL, '7529883721', '0', '8114', '$2y$10$Fbu6tIjeEVclK0HursD0tOJJctB7LfbrznMoSzzS9jGAQFFvHRGHO', NULL, '1', NULL, NULL, '1', '2019-07-09 09:14:04', '2019-07-09 09:14:17', NULL),
(18, 'ram', 'sham', NULL, '8054506559', '0', '0', '$2y$10$sLdgeJLM8Y5jP425j7g.c.twVa8YRqCjeCzqQCE6bzaVAwz0gc/5G', NULL, '1', 'dbyL_C07ISs:APA91bGj1-y6Xs_hDXu5YSx_U41XbDyW0TQmP8MClwi7GiwkXDgLmy2hXbuxZsJ7mGPQ7NaAhJH4iFQQHbjKeg9-4cSEmV1o5Li0AI6Cj8dU4Y-Y2X2EAtG9E2QoUdSPvmXLVFYSPtpn', NULL, '1', '2019-07-09 09:45:59', '2019-07-09 09:46:32', NULL),
(19, 'ram', 'sham', NULL, '80545011529', '0', '5119', '$2y$10$UirB7VpIt8Dz4vDHRHd1zOELAeOMTmsgjPvsnFkoHVexMUjV5/Zh2', NULL, '1', NULL, NULL, '1', '2019-07-09 09:51:57', '2019-07-09 09:51:57', NULL),
(20, 'ram', 'sham', NULL, '70545011529', '0', '0', '$2y$10$tZsBKFjusZ8XIbBS/LuQiOmmvPn8qUC7HDn45MQUtc7ns6rrRIfGa', NULL, '1', 'dbyL_C07ISs:APA91bGj1-y6Xs_hDXu5YSx_U41XbDyW0TQmP8MClwi7GiwkXDgLmy2hXbuxZsJ7mGPQ7NaAhJH4iFQQHbjKeg9-4cSEmV1o5Li0AI6Cj8dU4Y-Y2X2EAtG9E2QoUdSPvmXLVFYSPtpn', NULL, '1', '2019-07-09 09:52:18', '2019-07-09 09:55:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_detail` text COLLATE utf8mb4_unicode_ci,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `governorate_id` int(10) UNSIGNED DEFAULT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `default` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Yes, 1->no',
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_addresses`
--

INSERT INTO `user_addresses` (`id`, `user_id`, `name`, `address`, `address_detail`, `comment`, `latitude`, `longitude`, `governorate_id`, `city_id`, `section_id`, `default`, `params`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'testing', 'hjgjjdjkhf', NULL, 'comment', '98.456', '78.54', 1, 2, 3, '1', NULL, '1', '2019-06-07 08:24:05', '2019-06-28 03:57:00', NULL),
(2, 2, '1883', 'hdsjh', NULL, 'comment', NULL, NULL, 1, 2, 4, '0', NULL, '1', '2019-06-07 15:27:01', '2019-06-28 03:56:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_drivers`
--

CREATE TABLE `user_drivers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `parent_user_id` int(10) UNSIGNED NOT NULL,
  `vehicle_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `license_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_drivers`
--

INSERT INTO `user_drivers` (`id`, `user_id`, `parent_user_id`, `vehicle_number`, `license_number`, `params`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 2, '465+65+65', '+6+654654654', NULL, '0', '2019-06-24 08:50:33', '2019-06-24 08:50:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_suppliers`
--

CREATE TABLE `user_suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `vehicle_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `governorate_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `section_ids` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_suppliers`
--

INSERT INTO `user_suppliers` (`id`, `user_id`, `vehicle_number`, `license_number`, `latitude`, `longitude`, `governorate_id`, `city_id`, `section_ids`, `params`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, '987897987', '897897989', '92.7', '94.3', 1, 2, '4', NULL, '1', '2019-07-06 08:02:35', '2019-07-10 11:06:23', NULL),
(2, 2, 'fsfffdsdf', 'dfsdsdfsdf', NULL, NULL, 1, 2, '{\"4\",\"5\"}', NULL, '1', '2019-07-08 00:05:40', '2019-07-08 00:05:40', NULL),
(5, 2, '978897789978', '98979494', NULL, NULL, 1, 2, '{\"4\",\"5\",\"3\"}', NULL, '1', '2019-07-08 00:13:04', '2019-07-08 00:13:04', NULL),
(6, 11, '9788977899das78', '989794sda94', NULL, NULL, 1, 2, '{\"4\",\"5\",\"3\"}', NULL, '1', '2019-07-08 04:23:20', '2019-07-08 04:23:20', NULL),
(7, 12, '9788977899das781', '989794sda941', '92.7', '94.3', 1, 2, '{\"4\",\"5\",\"3\"}', NULL, '1', '2019-07-08 04:24:01', '2019-07-08 04:24:01', NULL),
(8, 13, '9788977899das7811', '989794sda9412', '92.7', '94.3', 1, 2, '{\"4\",\"5\",\"3\"}', NULL, '1', '2019-07-08 04:34:32', '2019-07-08 04:34:32', NULL),
(9, 14, '9788977899das7811s', '989794sda9412s', NULL, NULL, 1, 2, '[\"4\",\"5\",\"3\"]', NULL, '1', '2019-07-08 08:50:24', '2019-07-08 08:50:24', NULL),
(10, 16, NULL, NULL, NULL, NULL, 1, 2, '[\"4\",\"5\",\"3\"]', NULL, '1', '2019-07-09 00:25:50', '2019-07-09 00:25:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_supplier_favourites`
--

CREATE TABLE `user_supplier_favourites` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_supplier_service_rates`
--

CREATE TABLE `user_supplier_service_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `rate_cards` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_supplier_service_rates`
--

INSERT INTO `user_supplier_service_rates` (`id`, `supplier_id`, `service_id`, `rate_cards`, `params`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 4, '{\"item 1\":\"145\",\"Item 2\":\" 1454\",\"Item 3\":\"144\"}', NULL, '1', '2019-07-10 03:56:56', '2019-07-11 07:06:27', NULL),
(2, 3, 4, '{\"item 1\":\"145\",\"Item 2\":\" 1454\",\"Item 3\":\"144\"}', NULL, '1', '2019-07-10 03:59:38', '2019-07-11 07:06:31', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_email_unique` (`email`);

--
-- Indexes for table `configurations`
--
ALTER TABLE `configurations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gas_notifications`
--
ALTER TABLE `gas_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gas_notifications_user_id_index` (`user_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_customer_id_index` (`user_customer_id`),
  ADD KEY `orders_user_customer_address_id_index` (`user_customer_address_id`),
  ADD KEY `orders_service_id_index` (`service_id`),
  ADD KEY `orders_user_supplier_id_index` (`user_supplier_id`);

--
-- Indexes for table `order_children`
--
ALTER TABLE `order_children`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_children_order_id_index` (`order_id`),
  ADD KEY `order_children_service_id_index` (`service_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_mobile_number_unique` (`mobile_number`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_addresses_user_id_index` (`user_id`),
  ADD KEY `user_addresses_governorate_id_index` (`governorate_id`),
  ADD KEY `user_addresses_city_id_index` (`city_id`),
  ADD KEY `user_addresses_section_id_index` (`section_id`);

--
-- Indexes for table `user_drivers`
--
ALTER TABLE `user_drivers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_drivers_vehicle_number_unique` (`vehicle_number`),
  ADD UNIQUE KEY `user_drivers_license_number_unique` (`license_number`),
  ADD KEY `user_drivers_user_id_index` (`user_id`),
  ADD KEY `user_drivers_parent_user_id_index` (`parent_user_id`);

--
-- Indexes for table `user_suppliers`
--
ALTER TABLE `user_suppliers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_suppliers_vehicle_number_unique` (`vehicle_number`),
  ADD UNIQUE KEY `user_suppliers_license_number_unique` (`license_number`),
  ADD KEY `user_suppliers_user_id_index` (`user_id`);

--
-- Indexes for table `user_supplier_favourites`
--
ALTER TABLE `user_supplier_favourites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_supplier_favourites_supplier_id_index` (`supplier_id`),
  ADD KEY `user_supplier_favourites_customer_id_index` (`customer_id`);

--
-- Indexes for table `user_supplier_service_rates`
--
ALTER TABLE `user_supplier_service_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_supplier_service_rates_supplier_id_index` (`supplier_id`),
  ADD KEY `user_supplier_service_rates_service_id_index` (`service_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `configurations`
--
ALTER TABLE `configurations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gas_notifications`
--
ALTER TABLE `gas_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=238;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `order_children`
--
ALTER TABLE `order_children`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_drivers`
--
ALTER TABLE `user_drivers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_suppliers`
--
ALTER TABLE `user_suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_supplier_favourites`
--
ALTER TABLE `user_supplier_favourites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_supplier_service_rates`
--
ALTER TABLE `user_supplier_service_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `gas_notifications`
--
ALTER TABLE `gas_notifications`
  ADD CONSTRAINT `gas_notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_user_customer_address_id_foreign` FOREIGN KEY (`user_customer_address_id`) REFERENCES `user_addresses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_user_customer_id_foreign` FOREIGN KEY (`user_customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_user_supplier_id_foreign` FOREIGN KEY (`user_supplier_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_children`
--
ALTER TABLE `order_children`
  ADD CONSTRAINT `order_children_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_children_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD CONSTRAINT `user_addresses_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_addresses_governorate_id_foreign` FOREIGN KEY (`governorate_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_addresses_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_drivers`
--
ALTER TABLE `user_drivers`
  ADD CONSTRAINT `user_drivers_parent_user_id_foreign` FOREIGN KEY (`parent_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_drivers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_suppliers`
--
ALTER TABLE `user_suppliers`
  ADD CONSTRAINT `user_suppliers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_supplier_favourites`
--
ALTER TABLE `user_supplier_favourites`
  ADD CONSTRAINT `user_supplier_favourites_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_supplier_favourites_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_supplier_service_rates`
--
ALTER TABLE `user_supplier_service_rates`
  ADD CONSTRAINT `user_supplier_service_rates_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_supplier_service_rates_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
