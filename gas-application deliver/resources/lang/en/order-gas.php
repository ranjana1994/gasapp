<?php

return [
    'title' => 'order gas',
    'gas' => 'Gas',
    'button' => 'Cylinder Quantity',
    'heading' => 'Deliver the Order to Address',
    'supplier' => 'Supplier Phone Number',
    'level1' => 'Home Panaton Address line-1',
    'level2' => 'Home Panaton Address line-2',
    'level3' => 'Your Supplier Phone No.',
    'confirm' => 'Confirm The Order',
];