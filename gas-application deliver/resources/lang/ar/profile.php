 <?php
return [
    'my_profile' =>  'ملفي',
    'first_name' =>  'الاسم الکامل',
    'last_name' =>  'الكنية',
    'phone' =>  'رقم الموبایل',
    'language' =>  'تغییر اللغة',
    'save' =>  'الرجوع',
];