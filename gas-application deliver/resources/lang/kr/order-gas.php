 <?php
return [
    'title' => 'داواکردنی',
    'gas' => 'Xaz',
    'button' => 'Çelê Cylinder',
    'heading' => 'ژ.مۆبایلی بریکارەکەت لێرە بنوسە ئەگەر بریکارێک دەناسی',
    'supplier' => 'ژمارەی مۆبایلی بریکار',
    'level1' => 'Navnîşa Panaton Serûpel-1',
    'level2' => 'Navnîşa Panaton Serûpel-2',
    'level3' => 'Telefon No Supplier',
    'address' => 'محمد احمد علی',
    'confirm' => 'دووپاتکردنەوەی داواکاریەکە',
     
];