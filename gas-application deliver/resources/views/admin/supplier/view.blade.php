@extends('layouts.admin-data')
@section('content')
<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>{{$title}}</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li  class="breadcrumb-item">
                        <i class="icon-home"></i>
                        <a href="{{url('/admin')}}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li class="breadcrumb-item">
                        <i class="icon-users"></i>
                        <a class="ajaxify" href="{{$listUrl}}">List {{$title}}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <i class="icon-users"></i>
                        <span>View {{$title}}</span>
                    </li>
                </ol>
            </div>
        </div>
        <!-- Header end -->
        <div class="row">
            <!-- start col-lg-9 -->
            <div class="col-xl-12 col-lg-8">
                <!-- Nav tabs -->
                <div class="tab-header">
                    <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Personal Info</a>
                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#project" role="tab">Drivers</a>
                            <div class="slide"></div>
                        </li>
                    </ul>
                </div>
                <!-- end of tab-header -->
                <div class="tab-content">
                    <div class="tab-pane active" id="personal" role="tabpanel">
                        <div class="card">
                            <div class="card-header"><h5 class="card-header-text">About Supplier</h5>
                                <a id="edit-btn" href="{{$editUrl}}" type="button" class="btn btn-primary waves-effect waves-light f-right" >
                                    <i  class="icofont icofont-edit"></i>
                                </a>
                            </div>
                            <div class="card-block">
                                <div class="view-info">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="general-info">
                                                <div class="row">
                                                    <div class="col-lg-12 col-xl-6">
                                                        <table class="table m-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Name</th>
                                                                    <td>{{$data->name}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Email</th>
                                                                    <td>{{$data->email}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mobile Number</th>
                                                                    <td>{{$data->mobile_number}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">User Role</th>
                                                                    <td>{{implode(',',$data->getRoleNames()->toArray())}}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of row -->
                                            </div>
                                            <!-- end of general info -->
                                        </div>
                                        <!-- end of col-lg-12 -->
                                    </div>
                                    <!-- end of row -->
                                </div>
                            </div>
                            <!-- end of card-block -->
                        </div>
                        <!-- end of card-->
                        <!-- end of row -->
                    </div>
                    <div class="tab-pane" id="project" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-header-text">Drivers Details</h5>
                                <a type="button" href="{{route('drivers.create')}}" class="btn btn-primary waves-effect waves-light f-right">
                                    + ADD Drivers</a>
                            </div>
                            <!-- end of card-header  -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="project-table">
                                        <div class="table-responsive">
                                            <table class="table dt-responsive table-striped table-bordered nowrap" id="tblData" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <?php foreach ($driver_fields as $field): ?>
                                                            <th class="text-center txt-primary">{{$field}}</th>
                                                        <?php endforeach; ?>
                                                        <th class="text-center txt-primary">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- Container-fluid ends -->
</div>
<style type="text/css">
    .md-tabs .nav-item {
        width: calc(100%/2);
    }

    .nav-tabs .slide {
        width: calc(100% /2);
    }
    .table-responsive {
        padding: 25px;
    }
</style>
<script type="text/javascript" charset="utf-8" async defer>

//ajax header need for deleted and updating data

    var table;
//datatables serverSide
    $('document').ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        table = $('#tblData').DataTable({
        stateSave: true,
                responsive: true,
                processing: true,
                serverSide: true,
                order: [0, 'desc'],
                ajax: '{{route("drivers.index")}}',
                columns: [
                {data: 'id', name: 'id'},
<?php foreach ($driver_fields as $field): ?>
                    {data: "{{$field}}", name: "{{$field}}"},
<?php endforeach; ?>
                {data: 'action', name: 'action', orderable: false, searchable: false}
                ], "drawCallback": function (settings) {
        $('.change-state').bootstrapToggle();
        }
    });
    // table.draw(false);

//deleting data
    $('#tblData').on('click', '.btnDelete[data-remove]', function (e) {
        e.preventDefault();
        var url = $(this).data('remove');
        swal({
            title: "Are you sure want to remove this item?",
            text: "Data will be Temporary Deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Confirm",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: 'DELETE',
                            dataType: 'json',
                            data: {method: '_DELETE', submit: true},
                            success: function (data) {
                                if (data == 'Success') {
                                    swal("Deleted!", "Category has been deleted", "success");
                                    table.ajax.reload(null, false);
                                }
                            }
                        });
                    } else {

                        swal("Cancelled", "You Cancelled", "error");
                    }

                });
    });


    $('#tblData').on('change', '.change-state', function (e) {
    e.preventDefault();
            var url = "<?= url(config('app.admin_prefix') . '/user/changestate/') ?>" + '/' + $(this).attr('data-id');
            var checked = $('#change-state-' + $(this).attr('data-id')).prop('checked');
//            alert($('#change-state-' + $(this).attr('data-id')).prop('checked'));
            swal({
            title: "Are you sure want to change Vote status",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Confirm",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,
                    closeOnCancel: true
            },
                    function (isConfirm) {
                    if (isConfirm) {
                    $.ajax({
                    url: url,
                            type: 'GET',
                            datatype: 'json',
                            data: {checked: checked},
                            success: function (data) {
                            console.log(data);
                                    table.ajax.reload(null, false);
                            }

                    });
                    }
                    });
    });
    }
    );

</script>
@endsection