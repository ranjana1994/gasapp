@extends('layouts.admin-data')
@section('content')
<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <!-- Row Starts -->
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="main-header">
                    <h4>Application roles</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item">
                            <a href="<?= url(config('app.admin_prefix').'/dashboard')?>">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="<?= url(config('app.admin_prefix').'/roles')?>">Roles</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">List</a></li>
                    </ol>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="portlet-body card-block">
                        <form role="form" id="frmDataEdit" action="{{$action}}" style="padding: 0 5%;">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="id" class="control-label">
                                    ID
                                </label>
                                <input type="text" class="form-control" id="id" name="id" disabled  value="<?= isset($data->id) ? $data->id : '' ?>">
                            </div>  
                            <div class="form-group">
                                <label for="name" class="control-label">
                                    name<span class="required">*</span>
                                </label>
                                <input type="text" class="form-control" id="name" name="name"  value="<?= isset($data->name) ? $data->name : '' ?>">
                                <p class=" text-danger hidden"></p>
                            </div>  
                            <div class="form-group">
                                <label for="permission" class="control-label">
                                    permission<span class="required">*</span>
                                </label>
                                <br>
                                <input type="checkbox" id="selectall" onClick="selectAll(this)" />
                                <label for="selectall">Select All</label>
                                <br>
                                <?php
                                foreach ($permissions as $k => $permission):
                                    ?>
                                    <div class='col-md-4'>
                                        <input id="permission<?= $k ?>" type="checkbox" name="permission[]" value="<?= $permission->name ?>" <?= isset($data) ? ($data->hasPermissionTo($permission->name) ? 'checked="checked"' : '') : '' ?>>
                                        <label for="permission<?= $k ?>"><?= str_replace('api/', '', $permission->name) ?></label>
                                    </div>
                                <?php endforeach; ?>
                                <p class=" text-danger hidden"></p>
                            </div>
                        </form>
                        <br>
                        <br>
                        <div class="bt">
                            <a href="{{$listUrl}}" class="ajaxify btn btn-default" id="cancel" style="margin: 6%;"  >Cancel</a>
                            <button type="button" class="btn btn-primary" id="btnUpdate"><i class="glyphicon glyphicon-save"></i>&nbsp;Save</button>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="utf-8" async defer>
    function selectAll(source) {
        checkboxes = document.getElementsByName('permission[]');
        for (var i in checkboxes)
            checkboxes[i].checked = source.checked;
    }

    $('#btnUpdate').on('click', function (e) {
        e.preventDefault();
        var url = $('#frmDataEdit').attr('action');
        var frm = $('#frmDataEdit');
        var type = $('#id').val() == '' ? 'POST' : 'PUT';
        $.ajax({
            type: type,
            url: url,
            dataType: 'json',
            data: frm.serialize(),
            success: function (data) {
                // console.log(data);
                $('#frmDataEdit .text-danger').each(function () {
                    $(this).addClass('hidden');
                });
                if (data.errors) {
                    $.each(data.errors, function (index, value) {
                        $('#frmDataEdit #' + index).parent().find('.text-danger').text(value);
                        $('#frmDataEdit #' + index).parent().find('.text-danger').removeClass('hidden');
                    });
                }
                if (data.success == true) {
                    swal({title: "Success!", text: "{{$actionMessage}}", type: "success"},
                            function () {
                                $('#cancel')[0].click();
                            }
                    );
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                alert('Please Reload to read Ajax');
            }
        });
    });
</script>
@endsection