{{csrf_field()}}
<div class="form-group">
    <label for="id" class="control-label">
        ID
    </label>
    <input type="text" class="form-control" id="id" name="id" disabled  value="<?= isset($data->id) ? $data->id : '' ?>">
</div> 
<?php
//dd(\Request()->path());
foreach ($inputTypeTexts as $input => $inputValue):
    $label = ucwords(str_replace('_', ' ', $input));
    ?>
    <div class="form-group">
        <label for="<?= $input ?>" class="control-label">
            <?= $label ?> 
            <?= (strpos($inputValue, 'required') !== false) ? '<span class="required">*</span>' : '' ?>
        </label>
        <?php
        if (strpos($inputValue, 'in') !== false):
            $inputValue = (strpos($inputValue, 'required|') !== false) ? str_replace('required|', '', $inputValue) : $inputValue;
            $inputValueAr = explode(',', str_replace('in:', '', $inputValue));
            ?>
            <?php if ($input == 'role_customer'): ?>
                <div class="form-control form-radio" id="<?= $input ?>"  >
                    <?php foreach ($inputValueAr as $ar): ?>
                        <div class="radio radiofill">
                            <label>
                                <input type="radio" name="<?= $input ?>" value="<?= $ar ?>" <?= isset($data) ? (($data->hasRole($ar)) ? 'checked="checked"' : '') : '' ?>>
                                <i class="helper"></i><?= $ar ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php elseif ($input == 'role'): ?>
                <div class="form-control form-radio" id="<?= $input ?>"  >
                    <?php foreach ($inputValueAr as $ar): ?>
                        <div class="radio radiofill">
                            <label>
                                <input type="radio" name="<?= $input ?>" value="<?= $ar ?>" <?= isset($data) ? (($data->hasRole($ar)) ? 'checked="checked"' : '') : '' ?>>
                                <i class="helper"></i><?= $ar ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php elseif ($input == 'role_id'): ?>
                <div class="form-control form-radio" id="<?= $input ?>"  >
                    <?php
                    $inputValueAr = \Spatie\Permission\Models\Role::where('guard_name', 'api')->where('name', 'like', '%Supplier%')->get();
                    foreach ($inputValueAr as $ar):
                        ?>
                        <div class="radio radiofill">
                            <label>
                                <input type="radio" name="<?= $input ?>" value="<?= $ar->id ?>" <?= isset($data) ? (($data->$input == $ar->id) ? 'checked="checked"' : '') : '' ?>>
                                <i class="helper"></i><?= $ar->name ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php elseif ($input == 'role_supplier'): ?>
                <div class="form-control" id="<?= $input ?>"  >
                    <?php foreach ($inputValueAr as $ar): ?>
                        <div class="checkbox-fade fade-in-success">
                            <label>
                                <input type="checkbox" name="<?= $input . '[]' ?>" value="<?= $ar ?>" <?= isset($data) ? (($data->hasRole($ar)) ? 'checked="checked"' : '') : '' ?>>
                                <span class="cr"><i class="cr-icon fa fa-check txt-success"></i></span> <?= $ar ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>            
            <?php else: ?>
                <select class="form-control" id="<?= $input ?>" name="<?= $input ?>" >
                    <option value="" disabled="">Please Select <?= $label ?> </option>
                    <?php foreach ($inputValueAr as $ar): ?>
                        <option value="<?= $ar ?>" <?= isset($data->$input) ? ($data->$input == $ar) ? 'selected="selected"' : '' : '' ?> > <?= $ar ?></option>
                    <?php endforeach; ?>
                </select>
            <?php endif; ?>
            <?php
        elseif (strpos($inputValue, 'mime') !== false):
            if (isset($data->$input)):
                $img = (strpos($data->$input, 'http') !== false) ? $data->$input : 'public/uploads/services/' . $data->$input;
                echo $imgSrc = ($data->$input != '') ? '<img src="' . url($img) . '" class="logo"/>' : 'Not Found';

            endif;
            ?>
            <br/>
            <input type="file" class="form-contro" id="<?= $input ?>" name="<?= $input ?>" accept="image/x-png,image/gif,image/jpeg"/>

        <?php elseif (strpos($input, 'location') !== false): ?>
            <div class="form-control" id="<?= $input ?>"  >
                <?php foreach (App\Models\Location::where('type', 'governorate')->get() as $ar): ?>
                    <div class="checkbox-fade fade-in-success">
                        <label>
                            <input type="checkbox" name="<?= $input . '[]' ?>" value="<?= $ar->id ?>" <?= (isset($data->$input) && $data->$input != null) ? ((in_array($ar->id, json_decode($data->$input))) ? 'checked="checked"' : '') : '' ?>>
                            <span class="cr"><i class="cr-icon fa fa-check txt-success"></i></span> <?= $ar->name ?>
                        </label>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php elseif (strpos($input, 'parent_id') !== false): ?>
            <?php if (strpos(\Request()->path(), 'service') !== false): ?>
                <select class="form-control" id="<?= $input ?>" name="<?= $input ?>" >
                    <option value="" disabled="">Please Select <?= $input ?> </option>
                    <option value="0" <?= isset($data->$input) ? (($data->$input == '0') ? 'selected="selected"' : '') : '' ?>>root</option>
                    <?php
                    foreach (App\Models\Service::where('parent_id', '0')->get() as $par):
                        ?>
                        <option value="<?= $par->id ?>" <?= isset($data->$input) ? ($data->$input == $par->id) ? 'selected="selected"' : '' : '' ?> > <?= $par->name ?></option>
                    <?php endforeach; ?>
                </select>
            <?php else: ?>
                <select class="form-control" id="<?= $input ?>" name="<?= $input ?>" >
                    <option value="" disabled="">Please Select <?= $type ?> </option>
                    <?php
                    $columnT = ($type == 'city') ? 'name_kr' : 'name';
                    foreach (App\Models\Location::where('type', $type)->get() as $location):
                        ?>
                        <option value="<?= $location->id ?>" <?= isset($data->$input) ? ($data->$input == $location->id) ? 'selected="selected"' : '' : '' ?> > <?= $location->$columnT ?></option>
                    <?php endforeach; ?>
                </select>
            <?php
            endif;
        elseif (strpos($inputValue, 'color') !== false):
            ?>
            <input type="text" class="form-control color" id="<?= $input ?>" name="<?= $input ?>"data-inline="true" value="<?= isset($data->$input) ? $data->$input : '' ?>"/>
        <?php elseif (strpos($input, 'tnc') !== false): ?>
            <textarea class="form-control color" id="<?= $input ?>" name="<?= $input ?>" data-inline="true" style="height: 120px;"><?= isset($data->$input) ? $data->$input : '' ?></textarea>
        <?php elseif (strpos($input, 'howto') !== false): ?>
            <textarea class="form-control color" id="<?= $input ?>" name="<?= $input ?>" data-inline="true" style="height: 120px;"><?= isset($data->$input) ? $data->$input : '' ?></textarea>
        <?php else: ?>
            <input type="text" class="form-control" id="<?= $input ?>" name="<?= $input ?>"  value="<?= isset($data->$input) ? $data->$input : old($input) ?>">
        <?php endif; ?>
        <p class=" text-danger hidden"></p>
    </div>
<?php endforeach; ?>
<script type="text/javascript">
    $(function () {
        $('.color').minicolors();
    });
</script>