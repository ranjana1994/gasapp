@extends('layouts.admin-data')
@section('content')
<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <!-- Row Starts -->
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="main-header">
                    <h4>{{$title}}</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li  class="breadcrumb-item">
                            <i class="icon-home"></i>
                            <a href="{{url('/admin')}}">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>

                        <li class="breadcrumb-item">
                            <i class="icon-users"></i>
                            <a class="ajaxify" href="{{$listUrl}}">List {{$title}}</a>
                        </li>
                        <li class="breadcrumb-item">
                            <i class="icon-users"></i>
                            <span>Edit {{$title}}</span>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="portlet-body card-block">
                        <form role="form" id="frmDataEdit" action="{{$action}}">
                            @include('admin.commonForm.Cform')
                            <input type="hidden" name="parent_user_id" value="{{$supplier}}">
                        </form>
                        <br>
                        <br>
                        <div class="bt">
                            <a href="{{url()->previous()}}" class="ajaxify btn btn-default" id="cancel">Cancel</a>
                            <button type="button" class="btn btn-primary" id="btnUpdate"><i class="glyphicon glyphicon-save"></i>&nbsp;Save</button>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="utf-8" async defer>
    $('#btnUpdate').on('click', function (e)    {
        e.preventDefault();
        var url = $('#frmDataEdit').attr('action');
        var frm = $('#frmDataEdit');
        var type = $('#id').val() == '' ? 'POST' : 'PUT';
        $.ajax({
            type: type,
            url: url,
            dataType: 'json',
            data: frm.serialize(),
            success: function (data) {
                // console.log(data);
                $('#frmDataEdit .text-danger').each(function () {
                    $(this).addClass('hidden');
                });
                if (data.errors) {
                    $.each(data.errors, function (index, value) {
                        $('#frmDataEdit #' + index).parent().find('.text-danger').text(value);
                        $('#frmDataEdit #' + index).parent().find('.text-danger').removeClass('hidden');
                    });
                }
                if (data.success == true) {
                    swal({title: "Success!", text: "{{$actionMessage}}", type: "success"},
                            function () {
                                $('#cancel')[0].click();
                            }
                    );
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                alert('Please Reload to read Ajax');
            }
        });
    });
</script>
@endsection