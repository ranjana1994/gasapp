@extends('layouts.front-data')
@section('content')
<!-- Styles -->
<style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: Arial, Helvetica, sans-serif;
        font-weight: 100;
        height: 100vh;
        margin: 0;
    }
    .container-fluid {
        padding: 10px !important;
    }
    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }
    .position-ref {
        position: relative;
    }
    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }
    .content {
        text-align: center;
    }
    .title {
        font-size: 84px;
    }
    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }
    .m-b-md {
        margin-bottom: 30px;
    }
    .container-fluid {
        padding:0px;
        margin:0px;
    }
    .wrapper {
        padding-bottom: 30px;
    }

    body.sidebar-mini.fixed {
        border-top: 10px solid #fff;
    }
</style>
<?php
//dd($left);
?>
<div class="container-fluid">
    <div id="demo" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ul class="carousel-indicators">
            <?php foreach ($slider as $ki => $data): ?>
                <li data-target="#demo" data-slide-to="<?= $ki ?>" class="<?= $ki == '0' ? 'active' : '' ?>"></li>
            <?php endforeach; ?>
        </ul>
        <!-- The slideshow -->

        <div class="carousel-inner">
            <?php foreach ($slider as $k => $data): ?>
                <div class="carousel-item <?= $k == '0' ? 'active' : '' ?>">
                    <img src="<?= url('public/uploads/settings/' . $data->image) ?>" alt="<?= $data->name ?>" width="100%" height="500">
                </div>
            <?php endforeach; ?>
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>
    </div>
</div>

<div class="row container-fluid" style="margin-top:20px;">

    <div class="col-md-3">
        <?php
        foreach ($left as $datal):
            if ($datal->type == 'video'):
                ?>
                <iframe width="100%" height="auto" style="margin-top:10px;width: 308px;height: 260px;" src="<?= $datal->link ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

                <?php
            elseif ($datal->type == 'image'):
                ?>
                <a target="_BLANK" href="<?= $datal->link ?>"><img class="img-thumbnail" style="max-width: 308px;" src="<?= url('public/uploads/settings/' . $datal->image) ?>"></a>
                <?php
            endif;
        endforeach;
        ?>
    </div>
    <div class="col-md-6">
        <a href="<?= url('entry') ?>" class="btn btn-success col-md-12" style="padding:30px 0px;font-size:25px;margin:20px 0px;"><span class="blink"><?= \App\Models\Configuration::getConfiguration('register_placeholder') ?></span></a>
        <a href="<?= url('voting') ?>" class="btn btn-warning col-md-12" style="padding:30px 0px;font-size:25px;margin:20px 0px;"><span class="blink"><?= \App\Models\Configuration::getConfiguration('vote_placeholder') ?></span></a>
        <?php
        foreach ($bottom as $datab):
            if ($datab->type == 'video'):
                ?>
                <iframe width="100%" height="auto" style="width: 640px;max-height: 600px;" src="<?= $datab->link ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

                <?php
            elseif ($datab->type == 'image'):
                ?>
                <a target="_BLANK" href="<?= $datab->link ?>"><img class="img-thumbnail" style="width: 640px;max-height: 600px;" src="<?= url('public/uploads/settings/' . $datab->image) ?>"></a>
                <?php
            endif;
        endforeach;
        ?>
    </div>
    <div class="col-md-3">
        <?php
        foreach ($right as $datar):
            if ($datar->type == 'video'):
                ?>
                <iframe width="100%" height="auto" style="margin-top:10px;width: 308px;height: 260px;" src="<?= $datar->link ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
                <?php
            elseif ($datar->type == 'image'):
                ?>
                <a target="_BLANK" href="<?= $datar->link ?>"><img class="img-thumbnail" style="max-width: 308px;" src="<?= url('public/uploads/settings/' . $datar->image) ?>"></a>
                    <?php
                endif;
            endforeach;
            ?>
    </div>

</div>

@endsection