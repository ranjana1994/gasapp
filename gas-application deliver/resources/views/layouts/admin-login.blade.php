<!--@include('includes.config')-->
<!DOCTYPE html>
<html lang="en">
    <head>
        @include('includes.head')
        <!-- Chartlist chart css -->
       	<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="{{ asset('public/Fw-icon.png') }}" type="image/png">
        
    </head>
    <body>
        <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
            @yield('content')
        </section>
        @include('includes.footer')
        <!-- Counter js  -->
        <script src="{{ asset('public/admin/js/jquery-3.1.1.min.js') }}"></script>
        <script src="{{ asset('public/admin/js/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('public/admin/js/tether.min.js') }}"></script>
        <script src="{{ asset('public/admin/plugins/waves/js/waves.min.js') }}"></script>
        <script src="{{ asset('public/admin/js/bootstrap.min.js') }}"></script>
        <!-- Echart-js -->
        <script src="{{ asset('public/admin/pages/elements.js') }}"></script>
        <!--<script src="{{ asset('admin/js/common-pages.js') }}"></script>-->
    </body>
</html>
