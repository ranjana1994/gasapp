<!--@include('includes.config')-->
<!DOCTYPE html>
<html lang="en">
    <head>
        @include('includes.head')
        <!-- Chartlist chart css -->
       	<!--<link href="{{ asset('admin/plugins/data-table/css/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css">-->
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
        <link rel="shortcut icon" href="{{ asset('public/Fw-icon.png') }}" type="image/png">
        <!-- Data Table Css -->
        <link rel="stylesheet" type="text/css" href="{{ asset('public/admin/plugins/data-table/css/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/admin/plugins/data-table/css/buttons.dataTables.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/admin/plugins/data-table/css/responsive.bootstrap4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{asset('public/sweetalert/sweetalert.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <!-- calender picker js script -->
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!--<link rel="stylesheet" href="/resources/demos/style.css">-->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

<!--       <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
       <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>-->

    </head>
    <body class="sidebar-mini fixed">
        <div class="wrapper">
            <div class="loader-bg">
                <div class="loader-bar">
                </div>
            </div>

            @include('includes.header')
            <!-- Sidebar chat end--> 
            @if (count($errors) > 0)
            <div class="content-wrapper alert alert-danger">
                <a class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @yield('content')
            @yield('modal')

        </div>

        @include('includes.footer')
        <script src="{{ asset('public/admin/js/menu.js') }}"></script>
        <!-- jQuery -->
        <!--<script src="//code.jquery.com/jquery.js"></script>-->
        <!-- DataTables -->
        <script src="{{asset('public/sweetalert/sweetalert.min.js')}}" type="text/javascript" charset="utf-8" async defer></script>
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <!--<script src="{{ asset('admin/js/common-pages.js') }}"></script>-->
        <script type="text/javascript">
$(window).on('load', function () {
    var $window = $(window);
    $('.loader-bar').animate({width: $window.width()}, 2000);
    setTimeout(function () {
        while ($('.loader-bar').width() == $window.width()) {
            removeloader();
            break;
        }
    }, 2500);

    //Welcome Message (not for login page)
    function notify(message, type) {
        $.growl({
            message: message
        }, {
            type: type,
            allow_dismiss: false,
            label: 'Cancel',
            className: 'btn-xs btn-inverse',
            placement: {
                from: 'bottom',
                align: 'right'
            },
            delay: 2500,
            animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
            },
            offset: {
                x: 30,
                y: 30
            }
        });
    }
    ;

    $('.loader-bg').fadeOut('slow');
    $('#delete').on('click', function (e) {
        e.preventDefault();
        var url = "<?= url(config('app.admin_prefix') . '/delete-all') ?>" + '/' + $(this).attr('data-id');
        swal({
            title: "Are you sure you want to Delete All ?",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-info",
            confirmButtonText: "Confirm",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: 'GET',
                            datatype: 'json',
//                            data: {checked: checked},
                            success: function (data) {
//                                console.log(data);
                                table.ajax.reload(null, false);
                            }

                        });
                    }
                });
    });

});
        </script>
        @yield('scripts')
    </body>
</html>
