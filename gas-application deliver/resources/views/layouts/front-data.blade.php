<html lang="en">
    <head>
        <title>{{config('app.name')}}</title>

        <link rel="shortcut icon" href="{{ asset('public/Fw-icon.png') }}" type="image/png">
        <!--        <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
                 Data Table Css 
                <link rel="stylesheet" type="text/css" href="{{ asset('admin/plugins/data-table/css/dataTables.bootstrap4.min.css') }}">
                <link rel="stylesheet" type="text/css" href="{{ asset('admin/plugins/data-table/css/buttons.dataTables.min.css') }}">
                <link rel="stylesheet" type="text/css" href="{{ asset('admin/plugins/data-table/css/responsive.bootstrap4.min.css') }}">-->
        <!--<link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert.css')}}">-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <!-- calender picker js script -->
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css'>


        <link rel="stylesheet" href="<?= url('public/css/style.css') ?>">
        <!-- jQuery library -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133012527-1"></script>
        <script>
window.dataLayer = window.dataLayer || [];
function gtag() {
    dataLayer.push(arguments);
}
gtag('js', new Date());

gtag('config', 'UA-133012527-1');
        </script>

    </head>
    <body class="sidebar-mini fixed">
        <div class="wrapper">
            <div class="loader-bg">
                <div class="loader-bar">
                </div>
            </div>
            <div class="content">
                <div class="col-md-8 offset-md-2">
                    @include('includes.flash_msg')
                </div>
            </div>
            @yield('content')
            @yield('modal')
        </div>


        <script src="{{ asset('public/admin/js/jquery-3.1.1.min.js') }}"></script>
        <script src="{{ asset('public/admin/js/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('public/admin/js/tether.min.js') }}"></script>

        <!-- Required Fremwork -->
        <script src="{{ asset('public/admin/js/bootstrap.min.js') }}"></script>

        <!-- waves effects.js -->
        <script src="{{ asset('public/admin/plugins/waves/js/waves.min.js') }}"></script>

        <!-- Scrollbar JS-->
        <script src="{{ asset('public/admin/plugins/slimscroll/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('public/admin/plugins/slimscroll/js/jquery.nicescroll.min.js') }}"></script>

        <!--classic JS-->
        <script src="{{ asset('public/admin/plugins/search/js/classie.js') }}"></script>

        <!-- notification -->
        <script src="{{ asset('public/admin/plugins/notification/js/bootstrap-growl.min.js') }}"></script>

        <!--<script src="{{ asset('admin/js/menu.js') }}"></script>-->
        <!-- DataTables -->
        <!--<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        @yield('scripts')
    </body>
</html>
