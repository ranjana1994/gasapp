<!--@include('includes.config')-->
<!DOCTYPE html>
<html lang="en">
    <head>

        @include('includes.head')
        <!-- Chartlist chart css -->
        <link rel="stylesheet" href="{{ asset('public/admin/plugins/charts/chartlist/css/chartlist.css') }}" type="text/css" media="all">
        <link rel="shortcut icon" href="{{ asset('public/Fw-icon.png') }}" type="image/png">
        <!-- Weather css -->
        <link href="assets/css/svg-weather.css" rel="stylesheet">


    </head>
    <body class="sidebar-mini fixed">
        <div class="wrapper">
            <div class="loader-bg">
                <div class="loader-bar">
                </div>
            </div>
            <!-- Navbar-->
            @include('includes.header')
            @if (count($errors) > 0)
            <div class="content-wrapper alert alert-danger">
                <a class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @yield('content')

        </div>
        @include('includes.footer')
        <script src="{{ asset('public/admin/js/menu.js') }}"></script>
        <!-- Counter js  -->
        <script src="{{ asset('public/admin/plugins/countdown/js/waypoints.min.js') }}"></script>
        <script src="{{ asset('public/admin/plugins/countdown/js/jquery.counterup.js') }}"></script>
        <!-- Echart-js -->
        <script src="{{ asset('public/admin/plugins/charts/echarts/js/echarts-all.js') }}"></script>
        <!-- Data Tabel js-->
        <script src="{{ asset('public/admin/plugins/data-table/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('public/admin/plugins/data-table/js/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('public/admin/plugins/data-table/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('public/admin/plugins/data-table/js/responsive.bootstrap4.min.js') }}"></script>
        <!-- custom js -->
        <script type="text/javascript" src="{{ asset('public/admin/pages/dashboard4.js') }}"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <script type="text/javascript">
$(window).on('load', function () {
    var $window = $(window);
    $('.loader-bar').animate({width: $window.width()}, 2000);
    setTimeout(function () {
        while ($('.loader-bar').width() == $window.width()) {
            removeloader();
            break;
        }
    }, 2500);
    //Welcome Message (not for login page)
    function notify(message, type) {
        $.growl({
            message: message
        }, {
            type: type,
            allow_dismiss: false,
            label: 'Cancel',
            className: 'btn-xs btn-inverse',
            placement: {
                from: 'bottom',
                align: 'right'
            },
            delay: 2500,
            animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
            },
            offset: {
                x: 30,
                y: 30
            }
        });
    }
    ;
    notify('Welcome to {{config('app.name')}} Control Panel', 'inverse');
    $('.loader-bg').fadeOut('slow');
});

        </script>
    </body>
</html>
