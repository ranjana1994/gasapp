
<!-- https://codepen.io/ncerminara/pen/quJpi -->
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="{{ asset('public/images/logo.png')}}" type="image/x-icon"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/frontend/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/frontend/custom-styles.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>

    <body>
        <div id="site-wrapper">  	
            <div id="site-canvas">   
                <header>
                    <div id="site-menu">			   
                        <div class="nav-logo">
                                <!-- <a href="#" class="toggle-nav"><i class="fa fa-times"></i></a> -->
                            <a href="index.html"><img class="logo" src="{{asset('public/images/logo.png')}}"></a>
                        </div>  

                        <ul>
                            <li><a href="{{route('home.profile')}}"><img src="{{asset('public/images/profile.png')}}">{{__('home-head.my_profile')}}</a></li>					
                            <li><a href="{{route('home.service')}}"><img src="{{asset('public/images/order.png')}}">{{__('home-head.my_order')}}</a></li>
                            <li><a href=""><img src="{{asset('public/images/terms.png')}}">{{__('home-head.term_condition')}}</a></li>
                            <li><a href="{{ route('user.logout')}}"> {{__('home-head.logout')}}</a></li>


                        </ul>

                        <div class="nav-footer">
                            <ul>
                                <li><a href=""><img src="{{asset('public/images/fb.png')}}"></a></li>
                                <li><a href=""><img src="{{asset('public/images/twitter.png')}}"></a></li>
                                <li><a href=""><img src="{{asset('public/images/youtube.png')}}"></a></li>
                            </ul>
                        </div>
                    </div> 

                    <div class="row header">
                        <div class="col"><a href="#" class="toggle-nav" id="nav-toggle"><i class="fa fa-bars"></i></a></div>
                        <div class="col"><a href=""><img src="{{asset('public/images/logo.png')}}"></a></div>
<!--                        <div class="col"><span data-toggle="modal" data-target="#myModal"><i class="fa fa-bell-o" aria-hidden="true"></i> <span class="badge btn btn-danger">7</span></span></div>-->
                    </div>
                </header>
                @yield('content')
            </div>	
        </div>
<!--        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                     Modal Header 
                    <div class="modal-header">
                        <h4 class="modal-title">Notifications</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                     Modal body 
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-8" p-l:5px>
                                <p>Supplier of 2 Ltr Petrol in your section.</p>
                                <button class="btn btn-success">View Detail</button>
                            </div>
                            <div class="col-xs-4">
                                <span class="badge btn btn-danger">2</span>
                                <img src="{{asset('public/images/map.png')}}">
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-8">
                                <p>Supplier of 2 Ltr Petrol in your section.</p>
                                <button class="btn btn-success">View Detail</button>
                            </div>
                            <div class="col-xs-4">
                                <span class="badge btn btn-danger">2</span>
                                <img src="{{asset('public/images/map.png')}}">
                            </div>
                        </div>
                        <hr>

                    </div>
                </div>
            </div>
        </div>-->
    </body>
    <footer>
        <script src="{{ asset('public/js/frontend/jquery-3.4.0.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('public/js/frontend/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('public/js/frontend/scripts.js')}}"></script>
    </footer>

</html>