<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
<a href="index.html"></a>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/> 
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="{{ asset('public/images/logo.png')}}"type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/frontend/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/frontend/custom-styles.css')}}">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="{{ asset('public/js/frontend/jquery-3.4.0.min.js')}}"></script>
	<title>Profile</title>
</head>

<body>
     
    @yield('content')
</body>
<footer>
	<script src="{{ asset('public/js/frontend/jquery-3.4.0.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('public/js/frontend/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('public/js/frontend/scripts.js')}}"></script>
</footer>
</html>