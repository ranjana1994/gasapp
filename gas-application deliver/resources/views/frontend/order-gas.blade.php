@extends('frontend.layouts.order-gas')
@section('content')
<style>
    #loading{
        display:none;
    }
</style>
<div id="site-wrapper">  	
    <div id="site-canvas" class="order-by-quantity order-gas-quant">   
        <header>			
            <div class="row header">
                <?php
                $name = 'name';
                if (app()->getLocale() == 'ar'):
                    $name .= '_ar';
                endif;
                if (app()->getLocale() == 'kr'):
                    $name .= '_kr';
                endif;
                ?>


                <h4 class="col text-center">{{$orderlist->$name}}</h4>
            </div>
        </header>     
        <div class="orders">   
            <div class="container">  
                <div class="row">	
                    <div class="col-md-12 order-quantity">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="chekit"><h5><strong>{{$orderlist->$name}}
                                        </strong></h5></div> 
                                <div class="edit-quantity"><button type="button" class="btn btn-default">{{$orderlist->$name}} Qty</button> <input type="number" placeholder="0"value="{{count($orderlist->name)}}"></div>
                            </div>			
                        </div>
                    </div>

                    <div class="col-md-12 order-quantity select-address">
                        <div class="row">		
                            <div class="col-md-12">				
                                <div class="chekit"><h5><strong>{{__('order-gas.heading')}}</strong></h5></div></div>	

                            <?php foreach ($currentuser->addresses as $addresses): ?>

                                <div class="col-md-12 order-quantity select-addresss">
                                    <div class="row">	
                                        <div class="col-md-10 col-xs-8">
                                            <h6>{{$addresses->name.' : '.$addresses->address}}</h6> </div>
                                        <div class="edit-quantity col-md-2 col-xs-4 text-right">   
                                            <p>
                                                <input type="radio" id="test2" name="radio-group" checked>
                                                <label for="test2"></label>
                                            </p>
                                        </div>
                                    </div>			
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="col-md-12 order-quantity profile">
                            <div class="row">						
                                <div class="col-md-12 no-padding">
                                    <div class="chekit"><h5><strong>{{__('order-gas.supplier')}}</strong></h5></div>
                                    <div  class="form-group input-group">
                                        <label for="usr"></label>						
                                        <input type="text" class="w-80 full form-control" id="usr24" name="search" placeholder="{{__('order-gas.level3')}}">

                                        <div class="input-group-append">
                                            <span class="input-group-text"><i id ="search"class="fa fa-search" aria-hidden="true"></i></span>
                                        </div>					
                                    </div>
                                    <div id="loading">
                                        <i class="fa fa-spinner fa-spin"></i>
                                    </div>
                                    <div id="loadd">

                                    </div>

                                    <!--<div class="form-group">
                                    <label for="usr"></label>							
                                    <input type="text" class="w-100 full form-control" id="usr25" name="username25" placeholder="Mohamad Ali Ahmed">						
                                    </div>-->
                                </div>			
                            </div>
                        </div>		
                    </div>

                    <div class="w-100">
                        <button type="button" class="back btn w-100 btn-primary">{{__('order-gas.confirm')}}</button>
                    </div>
                </div>   
            </div>

        </div>

    </div>
    <script type="text/javascript" charset="utf-8" async defer>
        $(document).on('click', '#search', function (e) {
<<<<<<< HEAD
            $("#loading").show();
            $("#loadd").text('');
            $("#loadd").show();
            var search = $("#usr24").val();
            var url = "<?= url('/number') ?>";
            $.ajax({
                url: url,
                type: 'GET',
                datatype: 'json',
                data: {search: search},
                success: function (data) {
                    if (data.errors) {
                        $("#loading").fadeOut();
                        $('#loadd').text(data.errors);
                        $("#loadd").delay(2000).fadeOut();
                        return;
                    }
                    $('#loadd').text(data.name);
                    $("#loading").delay(1000).fadeOut();
                }
=======
        $("#loading").show();
        var search = $("#usr24").val();
        var url = "<?= url('/number') ?>";
        $.ajax({
        url: url,
        type: 'GET',
        datatype: 'json',
        data: {search: search},
        success: function (data) {
        $('#loadd').text(data.name);
        $("#loading").delay(1000).fadeOut();
        }
>>>>>>> a70650fa084d6af1608e33d449874ec1d8df5510

            });

        });

    </script>
    @stop

