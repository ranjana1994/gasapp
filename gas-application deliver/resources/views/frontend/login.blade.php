@extends('frontend.layouts.login-head')
@section('content')
<div class="container-fluid"> 
    <header class="center">
        <img src="{{asset('public/images/logo-dark.png')}}">
        <h5>Log in to your account</h5>
    </header>
    <form class="login" id="frmDataEdit" role="form" method="POST" action="{{ route('login.user.auth') }}">
        {!! csrf_field() !!}
        <div class="wrap">		  
            <div>
                <div class="form-group {{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                    <label for="mobile_number">Phone Number</label>
                    <input id="mobile_number" type="text" name="mobile_number" class="cool" autocomplete="off">
                    <p class=" text-danger hidden"></p>
                </div>
            </div>

            <div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Password</label>
                    <input id="password" type="password" name="password" class="cool" autocomplete="off">
                    <p class=" text-danger hidden"></p>
                </div>
            </div>
            <div>
                <!--<a class="forgot-pwd" href="#">Forgot Password?</a>-->
            </div>
            <button type="button" id="login" class="btn btn-primary sign">Sign in</button>
        </div>
    </form>
</div>
<script type="text/javascript" charset="utf-8" async defer>
    $(function () {
        $('#login').on('click', function (e) {
            e.preventDefault();
            var url = $('#frmDataEdit').attr('action');
            var frm = $('#frmDataEdit');
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: frm.serialize(),
                success: function (data) {
                    // console.log(data);
                    $('#frmDataEdit .text-danger').each(function () {
                        $(this).addClass('hidden');
                    });
                    if (data.errors) {
                        $.each(data.errors, function (index, value) {
                            $('#frmDataEdit #' + index).parent().find('.text-danger').text(value);
                            $('#frmDataEdit #' + index).parent().find('.text-danger').removeClass('hidden');
                        });
                    }
                    if (data.success == true) {
                        location.reload(true);
                    }
                    if (data.error == true) {
                        alert('Some thing went wrong !');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    alert('Please Reload to read Ajax');
                }
            });
        });
    });
</script>
@stop

