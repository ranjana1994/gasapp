@extends('frontend.layouts.home-head')
@section('content')
<div class="container-fluid home">  
    <div class="container product-details">  
        <div class="row">
            <div class="col-md-12">
                <ul class="product-list">
                    <?php
                    $name = 'name';
                    if (app()->getLocale() == 'ar'):
                         $name .= '_ar';
                    endif;
                    $image = 'image';
                    if (app()->getLocale() == 'ar'):
                       $image .= '_ar';
                    endif;

                    if (app()->getLocale() == 'kr'):
                        $name .= '_kr';
                    endif;

                    if (app()->getLocale() == 'kr'):
                        $image .= '_kr';
                    endif;
                    ?>
                    @foreach ($services as $service)
                    <?php $route = (count($service->childern) > 0) ? route('home.order') : route('order.show', [$service->id]) ?>                    
                    <li><span class="product-icon"><a href="{{$route}}"><img src="{{asset('public/uploads/services/'.$service->$image)}}"></span> <h4>{{$service->$name}}</h4></a></li> 
                    @endforeach
                </ul>
                <button class="w-100 btn btn-danger">{{__('home-head.problem')}}</button>
            </div>
        </div>   
    </div>
</div>
@stop

