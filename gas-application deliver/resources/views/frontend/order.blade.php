@extends('frontend.layouts.order-head')
@section('content')
<div id="site-wrapper">  	
    <div id="site-canvas">   
        <header>
            <div class="row header">
                <h4 class="col text-center">{{__('my-order.title')}}</h4>
            </div>
        </header>


        <div class="container-fluid orders">  


            <div class="container">  

                <h5>{{__('my-order.order')}}</h5>
                <div class="row">

                    <div class="col-md-12 my-order">
                        <h6 class="text-center">Order ID #12345677</h6>	
                        <div class="order-inner">			
                            <p><span><i class="fa fa-calendar" aria-hidden="true"></i> 12-09-2018</span>  <span><i class="fa fa-clock-o" aria-hidden="true"></i> 11:57 AM | Friday</span></p>
                            <p>{{__('my-order.supplier_name')}}<strong>{{__('my-order.name')}}</strong></p>
                            <button type="button" class="w-100 btn btn-light text-left">2 Cylinder</button>
                            <span class="float-right delete"><i class="fa fa-trash" aria-hidden="true"></i></span>
                        </div>
                        <button class="w-70 btn btn-outline-danger">{{__('my-order.problem')}}</button>
                    </div>

                    <div class="col-md-12 my-order">
                        <h6 class="text-center">Order ID #12345677</h6>	
                        <div class="order-inner">			
                            <p><span><i class="fa fa-calendar" aria-hidden="true"></i> 12-09-2018</span>  <span><i class="fa fa-clock-o" aria-hidden="true"></i> 11:57 AM | Friday</span></p>
                            <p>{{__('my-order.supplier_name')}}<strong>{{__('my-order.name')}}</strong></p>
                            <button type="button" class="w-100 btn btn-light text-left">2 Cylinder</button>
                            <span class="float-right delete"><i class="fa fa-trash" aria-hidden="true"></i></span>
                        </div>
                        <button class="w-70 btn btn-outline-danger">{{__('my-order.problem')}}</button>
                    </div>

                    <div class="col-md-12 my-order">
                        <h6 class="text-center">Order ID #12345677</h6>	
                        <div class="order-inner">			
                            <p><span><i class="fa fa-calendar" aria-hidden="true"></i> 12-09-2018</span>  <span><i class="fa fa-clock-o" aria-hidden="true"></i> 11:57 AM | Friday</span></p>
                            <p>{{__('my-order.supplier_name')}}<strong>{{__('my-order.name')}}</strong></p>
                            <button type="button" class="w-100 btn btn-light text-left">2 Cylinder</button>
                            <span class="float-right delete"><i class="fa fa-trash" aria-hidden="true"></i></span>
                        </div>
                        <button class="w-70 btn btn-outline-danger">{{__('my-order.problem')}}</button>
                    </div>

                    <div class="col-md-12 my-order">
                        <h6 class="text-center">Order ID #12345677</h6>	
                        <div class="order-inner">			
                            <p><span><i class="fa fa-calendar" aria-hidden="true"></i> 12-09-2018</span>  <span><i class="fa fa-clock-o" aria-hidden="true"></i> 11:57 AM | Friday</span></p>
                            <p>{{__('my-order.supplier_name')}}<strong>{{__('my-order.name')}}</strong></p>
                            <button type="button" class="w-100 btn btn-light text-left">2 Cylinder</button>
                            <span class="float-right delete"><i class="fa fa-trash" aria-hidden="true"></i></span>
                        </div>
                        <button class="w-70 btn btn-outline-danger">{{__('my-order.problem')}}</button>
                    </div>
                    <div class="w-100">
                        <a type="button" class="back btn w-100 btn-primary" href="{{ route('home.dashboard')}}">{{__('my-order.back')}}</a>
                    </div>
                </div>   
            </div>

        </div>


    </div>	
</div>
@stop

