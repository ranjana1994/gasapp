@extends('frontend.layouts.profile-head')
@section('content')
<div id="site-wrapper">  	
    <div id="site-canvas" class="profile">   
        <header>			
            <div class="row header">
                <div class='col-md-2 col-lg-2'>
                    <h4 class="col text-center">{{__('profile.my_profile')}}</h4>
                </div>

                <div class='col-md-10 col-lg-10'>


                </div>
            </div>
        </header>     
        <div class="orders">   
            <div class="container">  
                <div class="row">
                    <div class="col-md-12">
                        <form class="login" id="frmDataEdit" role="form" method="POST" action="{{ route('profile.save') }}">
                            {!! csrf_field() !!}
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="fname">{{__('profile.first_name')}}</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="First Name" value='{{$users->name}}'>
                                <p class=" text-danger hidden"></p>
                            </div>
                            <div class="form-group">
                                <label for="lname">{{__('profile.last_name')}}</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value='{{$users->last_name}}'>
                                <p class=" text-danger hidden"></p>
                            </div>

                            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone">{{__('profile.phone')}}</label>
                                <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Phone" value="{{$users->mobile_number}}">
                                <p class=" text-danger hidden"></p>
                            </div>
                            <div class="form-group {{ $errors->has('language') ? ' has-error' : '' }}">
                                <label for="language">{{__('profile.language')}}</label>

                                <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                                        <span class="navbar-toggler-icon"></span>
                                    </button>

                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">


                                        <!-- Right Side Of Navbar -->
                                        <ul class="navbar-nav">
                                            <!-- Authentication Links -->
                                            @php $locale = session()->get('locale'); @endphp
                                            <li class="nav-item dropdown">
                                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                    @switch($locale)
                                                    @case('ar')
                                                    Arabic
                                                    @break
                                                    @case('kr')
                                                    Kurdish
                                                    @break
                                                    @default
                                                    English
                                                    @endswitch
                                                    <span class="caret"></span>
                                                </a>
                                                <div class="dropdown-menu " aria-labelledby="navbarDropdown">
                                                    <a class="dropdown-item" href="lang/en">  English</a>
                                                    <a class="dropdown-item" href="lang/ar">  Arabic</a>


                                                    <a class="dropdown-item" href="lang/kr">kurdish</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                </nav>

                                <button type="button" id="login"class="back btn btn-primary profile">{{__('profile.save')}}</button>
                            </div>

                        </form>

                    </div>	

                </div>		
                <div class="w-100">

                </div>
            </div>   
        </div>

    </div>


</div>	
<script type="text/javascript" charset="utf-8" async defer>
    $(function () {
    $('#login').on('click', function (e) {
    e.preventDefault();
    var url = $('#frmDataEdit').attr('action');
    var frm = $('#frmDataEdit');
    $.ajax({
    type: 'POST',
    url: url,
    dataType: 'json',
    data: frm.serialize(),
    success: function (data) {
    console.log(data);
    $('#frmDataEdit .text-danger').each(function () {
    $(this).addClass('hidden');
    });
    if (data.errors) {
    $.each(data.errors, function (index, value) {
    $('#frmDataEdit #' + index).parent().find('.text-danger').text(value);
    $('#frmDataEdit #' + index).parent().find('.text-danger').removeClass('hidden');
    });
    }
    if (data.success == true) {
     window.location.href = "http://localhost/gas-application/dashboard";

    }
    if (data.error == true) {
    alert('User not found');
    }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
    console.log(jqXHR);
    console.log(textStatus);
    console.log(errorThrown);
    alert('Please Reload to read Ajax');
    }
    });
    });
    });
</script>
@stop

