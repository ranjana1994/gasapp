@extends('frontend.layouts.profile-head')
@section('content')
 <div id="site-wrapper">  	
	<div id="site-canvas" class="order-by-quantity">   
		<header>			
			<div class="row header">
				<h4 class="col text-center">{{__('order-water.title')}}</h4>
			</div>
		</header>     
     
<div class="orders">   
<div class="container">  
	
	<div class="row">
	
		<div class="col-md-12 order-quantity">
			<div class="row">			
				<div class="col-xs-4">
					<img src="{{asset('public/images/plastic-bowl.png')}}">
				</div>			
				<div class="col-xs-8">
					<div class="chekit"><h5>{{__('order-water.plastic_bowl')}}</h5><input type="checkbox" id="chk1"/> <label for="chk1"></label></div>
					<div class="edit-quantity"><button type="button" class="btn btn-default">{{__('order-water.set_quantity')}}</button> <input type="text" placeholder="0"></input></div>
				</div>			
			</div>
		</div>
		
		<div class="col-md-12 order-quantity">
			<div class="row">			
				<div class="col-xs-4">
					<img src="{{asset('public/images/350-ml.png')}}">
				</div>			
				<div class="col-xs-8">
					<div class="chekit"><h5>{{__('order-water.size')}}<strong>350 ml</strong></h5><input type="checkbox" id="chk2"/> <label for="chk2"></label></div>
					<div class="edit-quantity"><button type="button" class="btn btn-default">{{__('order-water.set_quantity')}}</button> <input type="text" placeholder="0"></input></div>
				</div>			
			</div>
		</div>
		<div class="col-md-12 order-quantity">
			<div class="row">			
				<div class="col-xs-4">
					<img src="{{asset('public/images/500-ml.png')}}">
				</div>			
				<div class="col-xs-8">
					<div class="chekit"><h5>{{__('order-water.size')}}<strong>0.5 L</strong></h5><input type="checkbox" id="chk3"/> <label for="chk3"></label></div>
					<div class="edit-quantity"><button type="button" class="btn btn-default">{{__('order-water.set_quantity')}}</button> <input type="text" placeholder="0"></input></div>
				</div>			
			</div>
		</div>
		<div class="col-md-12 order-quantity">
			<div class="row">			
				<div class="col-xs-4">
					<img src="{{asset('public/images/500-ml.png')}}">
				</div>			
				<div class="col-xs-8">
					<div class="chekit"><h5>{{__('order-water.size')}}<strong>1.5 L</strong></h5><input type="checkbox" id="chk4"/> <label for="chk4"></label></div>
					<div class="edit-quantity"><button type="button" class="btn btn-default">{{__('order-water.set_quantity')}}</button> <input type="text" placeholder="0"></input></div>
				</div>			
			</div>
		</div>
		<div class="col-md-12 order-quantity">
			<div class="row">			
				<div class="col-xs-4">
					<img src="{{asset('public/images/5-gallon.png')}}">
				</div>			
				<div class="col-xs-8">
					<div class="chekit"><h5>{{__('order-water.size')}}<strong>5 {{__('order-water.galon')}}</strong></h5><input type="checkbox" id="chk5"/> <label for="chk5"></label></div>
					<div class="edit-quantity"><button type="button" class="btn btn-default">{{__('order-water.set_quantity')}}</button> <input type="text" placeholder="0"></input></div>
				</div>			
			</div>
		</div>
		<div class="col-md-12 order-quantity">
			<div class="row">			
				<div class="col-xs-4">
					<img src="{{asset('public/images/plastic-bowl.png')}}">
				</div>			
				<div class="col-xs-8">
					<div class="chekit"><h5>{{__('order-water.plastic_bowl')}}</h5><input type="checkbox" id="chk6"/> <label for="chk6"></label></div>
					<div class="edit-quantity"><button type="button" class="btn btn-default">{{__('order-water.set_quantity')}}</button> <input type="text" placeholder="0"></input></div>
				</div>			
			</div>
		</div>
			
		</div>
		
		<div class="w-100">
			<button type="button" class="back btn w-100 btn-primary">{{__('order-water.confirm_order')}}</button>
		</div>
	</div>   
</div>

</div>


</div>	
</div>



@stop

