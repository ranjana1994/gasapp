<?php
/**
 * header.php
 *
 * Author: phoenixcoded
 *
 * The first block of code used in every page of the template
 *
 */
$template = config('adminNav.template');
$template['active_page'] = str_replace('admin.', "", Route::currentRouteName());
$primary_nav = config('adminNav.primary_nav');
 
?>
<!-- Navbar-->
<header class="main-header-top hidden-print">
    <a href="{{ route('admin.dashboard') }}" class="logo">{{ config('app.name') }}</a>
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#!" data-toggle="offcanvas" class="sidebar-toggle"></a>
        <!-- Navbar Right Menu-->
        <div class="navbar-custom-menu">
            <ul class="top-nav">
                <!--Notification Menu-->

                <!--                <li class="dropdown pc-rheader-submenu message-notification search-toggle">
                                    <a href="#!" id="morphsearch-search" class="drop icon-circle txt-white">
                                        <i class="icofont icofont-search-alt-1"></i>
                                    </a>
                                </li>
                                <li class="dropdown notification-menu">
                                    <a href="#!" data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle">
                                        <i class="icon-bell"></i>
                                        <span class="badge badge-danger header-badge">9</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="not-head">You have <b class="text-primary">4</b> new notifications.</li>
                                        <li class="bell-notification">
                                            <a href="javascript:;" class="media">
                                                <span class="media-left media-icon">
                                                    <img class="img-circle" src="assets/images/avatar-1.png" alt="User Image">
                                                </span>
                                                <div class="media-body"><span class="block">Lisa sent you a mail</span><span class="text-muted block-time">2min ago</span></div></a>
                                        </li>
                                        <li class="bell-notification">
                                            <a href="javascript:;" class="media">
                                                <span class="media-left media-icon">
                                                    <img class="img-circle" src="assets/images/avatar-2.png" alt="User Image">
                                                </span>
                                                <div class="media-body"><span class="block">Server Not Working</span><span class="text-muted block-time">20min ago</span></div></a>
                                        </li>
                                        <li class="bell-notification">
                                            <a href="javascript:;" class="media"><span class="media-left media-icon">
                                                    <img class="img-circle" src="assets/images/avatar-3.png" alt="User Image">
                                                </span>
                                                <div class="media-body"><span class="block">Transaction xyz complete</span><span class="text-muted block-time">3 hours ago</span></div></a>
                                        </li>
                                        <li class="not-footer">
                                            <a href="#!">See all notifications.</a>
                                        </li>
                                    </ul>
                                </li>-->
                <!-- chat dropdown -->
                <!--                <li class="pc-rheader-submenu ">
                                    <a href="#!" class="drop icon-circle displayChatbox">
                                        <i class="icon-bubbles"></i>
                                        <span class="badge badge-danger header-badge blink">5</span>
                                    </a>
                                </li>-->
                <!-- window screen -->
                <li class="pc-rheader-submenu">
                    <a href="#!" class="drop icon-circle" onclick="javascript:toggleFullScreen()">
                        <i class="icon-size-fullscreen"></i>
                    </a>

                </li>
                <!-- User Menu-->
                <li class="dropdown">
                    <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle drop-image">
                        <span><img class="img-circle " src="{{ asset('public/favicon.png')}}" style="width:40px;" alt="User Image"></span>
                        <span><?= auth()->guard('admin')->user()->name ?> <i class=" icofont icofont-simple-down"></i></span>

                    </a>
                    <ul class="dropdown-menu settings-menu">
                        <!--<li><a href="#!"><i class="icon-settings"></i> Settings</a></li>-->
                        <!--<li><a href="#!"><i class="icon-user"></i> Profile</a></li>-->
                        <!--<li><a href="message.php"><i class="icon-envelope-open"></i> My Messages</a></li>-->
                        <li class="p-0">
                            <div class="dropdown-divider m-0"></div>
                        </li>
                        <!--<li><a href="lock-screen.php"><i class="icon-lock"></i> Lock Screen</a></li>-->
                        <li><a onclick="event.preventDefault();document.getElementById('logout-form').submit();" href="{{ route('logout') }}"><i class="icon-logout"></i> Logout</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </li>
            </ul>

            <!-- search -->
            <div id="morphsearch" class="morphsearch">
                <form class="morphsearch-form">

                    <input class="morphsearch-input" type="search" placeholder="Search..."/>

                    <button class="morphsearch-submit" type="submit">Search</button>

                </form>
                <div class="morphsearch-content">
                    <div class="dummy-column">
                        <h2>People</h2>
                        <a class="dummy-media-object">
                            <img class="round" src="https://0.gravatar.com/avatar/81b58502541f9445253f30497e53c280?s=50&d=identicon&r=G" alt="Sara Soueidan"/>
                            <h3>Sara Soueidan</h3>
                        </a>

                        <a class="dummy-media-object">
                            <img class="round" src="https://1.gravatar.com/avatar/9bc7250110c667cd35c0826059b81b75?s=50&d=identicon&r=G" alt="Shaun Dona"/>
                            <h3>Shaun Dona</h3>
                        </a>
                    </div>
                    <div class="dummy-column">
                        <h2>Popular</h2>
                        <a class="dummy-media-object">
                            <img src="assets/public/images/avatar-1.png" alt="PagePreloadingEffect"/>
                            <h3>Page Preloading Effect</h3>
                        </a>

                        <a class="dummy-media-object">
                            <img src="assets/images/avatar-1.png" alt="DraggableDualViewSlideshow"/>
                            <h3>Draggable Dual-View Slideshow</h3>
                        </a>
                    </div>
                    <div class="dummy-column">
                        <h2>Recent</h2>
                        <a class="dummy-media-object">
                            <img src="assets/images/avatar-1.png" alt="TooltipStylesInspiration"/>
                            <h3>Tooltip Styles Inspiration</h3>
                        </a>
                        <a class="dummy-media-object">
                            <img src="assets/images/avatar-1.png" alt="NotificationStyles"/>
                            <h3>Notification Styles Inspiration</h3>
                        </a>
                    </div>
                </div><!-- /morphsearch-content -->
                <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>
            </div>
            <!-- search end -->
        </div>
    </nav>
</header>
<!-- Side-Nav-->
<aside class="main-sidebar hidden-print " >
    <section class="sidebar" id="sidebar-scroll">
        <div class="user-panel">
            <div class="f-left image"><img src="{{ asset('public/favicon.png')}}" alt="User Image" class="img-circle"></div>
            <div class="f-left info">
                <p><?= auth()->guard('admin')->user()->name ?></p>
                <!--<p class="designation">UX Designer <i class="icofont icofont-caret-down m-l-5"></i></p>-->
            </div>
        </div>
        <!-- sidebar profile Menu-->
        <!--        <ul class="nav sidebar-menu extra-profile-list">
                    <li>
                        <a class="waves-effect waves-dark" href="profile.php">
                            <i class="icon-user"></i>
                            <span class="menu-text">View Profile</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li>
                        <a class="waves-effect waves-dark" href="javascript:void(0)">
                            <i class="icon-settings"></i>
                            <span class="menu-text">Settings</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li>
                        <a class="waves-effect waves-dark" href="javascript:void(0)">
                            <i class="icon-logout"></i>
                            <span class="menu-text">Logout</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>-->
        <!-- Sidebar Menu-->
        <?php
        if ($primary_nav) {
            ?>
            <ul class="sidebar-menu">
                <?php
                foreach ($primary_nav as $key => $link) {
                    $link_class = '';
                    $li_active = '';
                    $menu_link = '';
                    $template['active_page'] = isset(explode('.', $template['active_page'])[0]) ? explode('.', $template['active_page'])[0] : $template['active_page'];
                    // Get 1st level link's vital info
                    $url = (isset($link['url']) && $link['url']) ? url(config('app.admin_prefix') . '/' . $link['url']) : '#!';
                    $active = (isset($link['url']) && ($template['active_page'] == $link['url'])) ? ' active ' : '';
                    $icon = (isset($link['icon']) && $link['icon']) ? '<i class="' . $link['icon'] . '"></i>' : '';

                    // Check if the link has a submenu
                    if ($url != 'widget.php') {
                        if (isset($link['sub']) && $link['sub']) {
                            // Since it has a submenu, we need to check if we have to add the class active
                            // to its parent li element (only if a 2nd or 3rd level link is active)
                            foreach ($link['sub'] as $sub_link) {
                                if (in_array($template['active_page'], $sub_link)) {
                                    $li_active = ' class="active "';
                                    break;
                                }
                                // 3rd level links
                                if (isset($sub_link['sub']) && $sub_link['sub']) {
                                    foreach ($sub_link['sub'] as $sub2_link) {
                                        if (in_array($template['active_page'], $sub2_link)) {
                                            $li_active = ' class="active "';
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if ($template['active_page'] == 'widget.php') {
                            $li_active = ' class="active "';
                        }
                    }
                    if ($active == ' active '):
                        $li_active = ' class="active"';
                    endif;
                    ?>
                    <?php if ($url == 'header') { // if it is a header and not a link  ?>
                        <li class="nav-level"><?php echo $link['name']; ?></li>
                    <?php } else { // If it is a link  ?>
                        <li<?php echo $li_active; ?>>
                            <a  class="waves-effect waves-dark" href="<?php echo $url; ?>">
                                <?php
                                if (isset($link['sub']) && $link['sub']) {
                                    // if the link has a submenu     
                                    ?>
                                <?php } echo $icon; ?><span>
                                    <?php echo $link['name']; ?></span>
                                <?php if (isset($link['opt']) && $link['opt']) { // If the header has options set     ?><?php echo $link['opt']; ?><?php } ?>
                                <?php if ($url == '#!') { ?>
                                    <i class="icon-arrow-down"></i>
                                <?php } ?></a>
                            <?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu    ?>
                                <ul class="treeview-menu">
                                    <?php
                                    foreach ($link['sub'] as $sub_link) {
                                        $link_class = '';
                                        $li_active = '';
                                        $submenu_link = '';

                                        // Get 2nd level link's vital info
                                        $url = (isset($sub_link['url']) && $sub_link['url']) ? $sub_link['url'] : '#';
                                        $active = (isset($sub_link['url']) && ($template['active_page'] == $sub_link['url'])) ? 'active' : '';

                                        // Check if the link has a submenu
                                        if (isset($sub_link['sub']) && $sub_link['sub']) {
                                            // Since it has a submenu, we need to check if we have to add the class active
                                            // to its parent li element (only if a 3rd level link is active)
                                            foreach ($sub_link['sub'] as $sub2_link) {
                                                if (in_array($template['active_page'], $sub2_link)) {
                                                    $li_active = ' class="active treeview"';
                                                    break;
                                                }
                                            }
                                        }
                                        ?>

                                        <li<?php echo $li_active; ?>>
                                            <a href="<?php echo $url; ?>"class="waves-effect waves-dark <?php echo $active; ?> "><i class="icon-arrow-right"></i><span><?php echo $sub_link['name']; ?></span><?php if (isset($sub_link['opt']) && $sub_link['opt']) { // If the header has options set               ?><?php echo $sub_link['opt']; ?><?php } ?><?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?><i class="icon-arrow-down"></i><?php } ?></a>
                                            <?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?>
                                                <ul class="treeview-menu">
                                                    <?php
                                                    foreach ($sub_link['sub'] as $sub2_link) {
                                                        $link_class = '';
                                                        $li_active = '';
                                                        $submenu_link = '';
                                                        // Get 3rd level link's vital info
                                                        $url = (isset($sub2_link['url']) && $sub2_link['url']) ? $sub2_link['url'] : '#';
                                                        $active = (isset($sub2_link['url']) && ($template['active_page'] == $sub2_link['url'])) ? ' active' : '';
                                                        $submenu_link = 'waves-effect waves-dark';
                                                        if ($submenu_link || $active) {
                                                            $link_class = ' class="' . $submenu_link . $active . '"';
                                                        }
                                                        ?>
                                                        <li>
                                                            <a  href="<?php echo $url; ?>"<?php echo $link_class; ?> ><i class="icon-arrow-right"></i><?php echo $sub2_link['name']; ?><?php if (isset($sub2_link['sub']) && $sub2_link['sub']) { ?><i class="icon-arrow-down"></i><?php } ?></a>
                                                            <?php
                                                            if (isset($sub_link['sub']) && $sub_link['sub']) {
                                                                if ($sub2_link['name'] == 'Level Three' && $sub2_link['opt'] == 'Level Three') {
                                                                    ?>
                                                                    <ul class="treeview-menu">
                                                                        <?php
                                                                        foreach ($sub2_link['sub'] as $sub4_link) {
                                                                            // Get 4rd level link's vital info
                                                                            $url = (isset($sub4_link['url']) && $sub4_link['url']) ? $sub4_link['url'] : '#';
                                                                            $active = (isset($sub4_link['url']) && ($template['active_page'] == $sub4_link['url'])) ? ' class="active"' : '';
                                                                            ?>
                                                                            <li>
                                                                                <a href="<?php echo $url; ?>"<?php echo $active ?>><i class="icon-arrow-right"></i><?php echo $sub4_link['name']; ?></a>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>

                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        <?php } ?>
    </section>
</aside>
<!-- Sidebar chat start -->
<div id="sidebar" class="p-fixed header-users showChat">
    <div class="had-container">
        <div class="card card_main header-users-main">
            <div class="card-content user-box">

                <div class="md-group-add-on p-20">
                    <span class="md-add-on">
                        <i class="icofont icofont-search-alt-2 chat-search"></i>
                    </span>
                    <div class="md-input-wrapper">
                        <input type="text" class="md-form-control"  name="username" id="search-friends">
                        <label for="username">Search</label>
                    </div>

                </div>
                <div class="media friendlist-main">

                    <h6>Friend List</h6>

                </div>
                <div class="main-friend-list">
                    <div class="media friendlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">

                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-1.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Josephin Doe</div>
                            <span>20min ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="3" data-status="online" data-username="Alice"  data-toggle="tooltip" data-placement="left" title="Alice">
                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-2.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Alice</div>
                            <span>1 hour ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="7" data-status="offline" data-username="Michael Scofield" data-toggle="tooltip" data-placement="left" title="Michael Scofield">
                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-3.png" alt="Generic placeholder image">
                            <div class="live-status bg-danger"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Michael Scofield</div>
                            <span>3 hours ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="5" data-status="online" data-username="Irina Shayk" data-toggle="tooltip" data-placement="left" title="Irina Shayk">
                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-4.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Irina Shayk</div>
                            <span>1 day ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="6" data-status="offline" data-username="Sara Tancredi" data-toggle="tooltip" data-placement="left" title="Sara Tancredi">
                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-5.png" alt="Generic placeholder image">
                            <div class="live-status bg-danger"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Sara Tancredi</div>
                            <span>2 days ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-1.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Josephin Doe</div>
                            <span>20min ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-2.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Alice</div>
                            <span>1 hour ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">

                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-1.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Josephin Doe</div>
                            <span>20min ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-2.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Alice</div>
                            <span>1 hour ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip"  data-placement="left" title="Josephin Doe">

                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-1.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Josephin Doe</div>
                            <span>20min ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="3" data-status="online" data-username="Alice"  data-toggle="tooltip" data-placement="left" title="Alice">
                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-2.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Alice</div>
                            <span>1 hour ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">

                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-1.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Josephin Doe</div>
                            <span>20min ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">

                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-1.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Josephin Doe</div>
                            <span>20min ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">

                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-1.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Josephin Doe</div>
                            <span>20min ago</span>
                        </div>
                    </div>
                    <div class="media friendlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">

                        <a class="media-left" href="#!">
                            <img class="media-object img-circle" src="assets/images/avatar-1.png" alt="Generic placeholder image">
                            <div class="live-status bg-success"></div>
                        </a>
                        <div class="media-body">
                            <div class="friend-header">Josephin Doe</div>
                            <span>20min ago</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="showChat_inner">
    <div class="media chat-inner-header">
        <a class="back_chatBox">
            <i class="icofont icofont-rounded-left"></i> Josephin Doe
        </a>
    </div>
    <div class="media chat-messages">
        <a class="media-left photo-table" href="#!">
            <img class="media-object img-circle m-t-5" src="assets/images/avatar-1.png" alt="Generic placeholder image">
            <div class="live-status bg-success"></div>
        </a>
        <div class="media-body chat-menu-content">
            <div class="">
                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                <p class="chat-time">8:20 a.m.</p>
            </div>
        </div>
    </div>
    <div class="media chat-messages">
        <div class="media-body chat-menu-reply">
            <div class="">
                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                <p class="chat-time">8:20 a.m.</p>
            </div>
        </div>
        <div class="media-right photo-table">
            <a href="#!">
                <img class="media-object img-circle m-t-5" src="assets/images/avatar-2.png" alt="Generic placeholder image">
                <div class="live-status bg-success"></div>
            </a>
        </div>
    </div>
    <div class="media chat-reply-box">
        <div class="md-input-wrapper">
            <input type="text" class="md-form-control" id="inputEmail" name="inputEmail" >
            <label>Share your thoughts</label>
            <span class="highlight"></span>
            <span class="bar"></span>
        </div>

    </div>
</div>
<!-- Sidebar chat end-->