<?php
/**
 * script.php
 *
 * Author: phoenixcoded
 *
 * All vital JS scripts are included here
 *
 */
?>
<script src="{{ asset('public/admin/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('public/admin/js/tether.min.js') }}"></script>

<!-- Required Fremwork -->
<script src="{{ asset('public/admin/js/bootstrap.min.js') }}"></script>

<!-- waves effects.js -->
<script src="{{ asset('public/admin/plugins/waves/js/waves.min.js') }}"></script>

<!-- Scrollbar JS-->
<script src="{{ asset('public/admin/plugins/slimscroll/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('public/admin/plugins/slimscroll/js/jquery.nicescroll.min.js') }}"></script>

<!--classic JS-->
<script src="{{ asset('public/admin/plugins/search/js/classie.js') }}"></script>

<!-- notification -->
<script src="{{ asset('public/admin/plugins/notification/js/bootstrap-growl.min.js') }}"></script>

<!-- custom js -->
<script type="text/javascript" src="{{ asset('public/admin/js/main.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/pages/elements.js') }}"></script>
<!--<script src="{{ asset('admin/js/menu.js') }}"></script>-->

