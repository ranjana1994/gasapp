<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends BaseModel {

    use SoftDeletes;

    //

    public function user_customer_address_id() {
        return $this->hasOne('App\Models\UserAddress', 'id', 'user_customer_address_id');
    }

    public function user_customer_id() {
        return $this->hasOne('App\User', 'id', 'user_customer_id')->select('id', 'name', 'last_name', 'email', 'mobile_number');
    }

    public function service_id() {
        return $this->hasOne('App\Models\Service', 'id', 'service_id');
    }

    public function user_supplier_id() {
        return $this->hasOne('App\User', 'id', 'user_supplier_id');
    }

    public function order_child() {
        return $this->hasMany('App\Models\OrderChild', 'order_id', 'id')->with('service_id');
    }

}
