<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class OrderChild extends BaseModel {

//    use SoftDeletes;
    //

    public function service_id() {
        return $this->hasOne('App\Models\Service', 'id', 'service_id');
    }

}
