<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddress extends BaseModel {

    use SoftDeletes;

    //
    public function governorate() {
        return $this->hasOne('App\Models\Location', 'id', 'governorate_id');
    }

    public function city() {
        return $this->hasOne('App\Models\Location', 'id', 'city_id');
    }

    public function section() {
        return $this->hasOne('App\Models\Location', 'id', 'section_id');
    }

}
