<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserSupplier extends BaseModel {

    use SoftDeletes;
    
    public function governorate() {
        return $this->hasOne('App\Models\Location', 'id', 'governorate_id');
    }

    public function city() {
        return $this->hasOne('App\Models\Location', 'id', 'city_id');
    }

}
