<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Illuminate\Routing\Router;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleMiddleware {

    public function handle($request, Closure $next, $role) {

        if (!Auth::guard('admin')->check()) {
            return redirect()->route('admin.login');
            throw UnauthorizedException::notLoggedIn();
        }
        $roles = is_array($role) ? $role : explode('|', $role);
        $roles = \Spatie\Permission\Models\Role::where('guard_name', 'web-admin')->get()->pluck('name')->toArray();

//        dd($roles);
        $user = \App\AdminUser::find(Auth::guard('admin')->user()->id);
//        dd($user->getRoleNames());
//        dd($user->hasAnyRole($roles));
//        dd();
        if (!$user->hasAnyRole($roles)) {
            throw UnauthorizedException::forRoles($roles);
        }
//        dd($roles);
//        dd(\Request::route()->uri());
        if (\Request::route()->uri() != config('app.admin_prefix') . '/home'):
            try {
//                $user = Auth::user();
                if ($user->hasPermissionTo(\Request::route()->uri()) === false):
//                    \flash("You're not authorized to View this route " . \Request::route()->uri());
//                    dd("You're not authorized to View this route " . \Request::route()->uri());
//                    return redirect()->back()->withError("You're not authorized to View this route " . \Request::route()->uri());
                    return \Redirect::back()->withErrors(["You're not authorized to View this route " . \Request::route()->uri()]);
                endif;
            } catch (\Exception $ex) {
//                flash($ex->getMessage());
                dd($ex->getMessage());
                return redirect()->back();
            }
        endif;
        return $next($request);
    }

}
