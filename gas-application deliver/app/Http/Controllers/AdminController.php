<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller {

    public function __construct() {
        $roles = \Spatie\Permission\Models\Role::where('guard_name','web-admin')->get()->pluck('name')->toArray();
//        dd(implode(',', $roles));
        $this->middleware(['role:'.implode(',', $roles)]); 
//        dd(Auth::guard('admin')->user());
//        dd(auth()->guard('admin')->user());
    }

    public function dashboard() {

        $totalcustomers = User::select('*')->role(\GasApplication::getUserRoles('customer', 'ctg'))->count();
        $todaycustomers = User::select('*')->role(\GasApplication::getUserRoles('customer', 'ctg'))->whereDate('created_at',Carbon::today())->count();
       // dd($todaycustomers);
        $totalsuppliers = User::select('*')->role(\GasApplication::getUserRoles('supplier', 'ctg'))->count();
        
        $todaysuppliers = User::select('*')->role(\GasApplication::getUserRoles('supplier', 'ctg'))->whereDate('created_at',Carbon::today())->count();
           //dd($todaysuppliers);
        return view('admin.dashboard', ['totalcustomers' => $totalcustomers, 'todaycustomers' => $todaycustomers, 'totalsuppliers' => $totalsuppliers, 'todaysuppliers' => $todaysuppliers]);
    }

    public function index() {
        return view('admin.admin-users');
    }

    public function deleteAll($model) {
        \DB::table($model)->truncate();
        return response()->json(['status' => true]);
    }

}
