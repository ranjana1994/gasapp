<?php

namespace App\Http\Controllers\API;

use App\Models\Location;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use Exception;

class LocationController extends ApiController {

    public function getItem(Request $request, $type, $parentid = null) {
        //Validating attributes
        $validateAttributes = parent::validateHeadersOnly($request, 'GET', $this->requiredParams);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        if (!in_array($type, ['governorate', 'city', 'section'])):
            return self::error('Please use valid type.', 422);
        endif;
        $model = Location::where('type', $type);
        if ($parentid != null)
            $model->where('parent_id', $parentid);
        if (count($model))
            return parent::success($model->where('state', '1')->get());
        else
            return parent::error('No Data Found');
    }

}
