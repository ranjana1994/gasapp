<?php

namespace App\Http\Controllers\API;

use App\Models\Service;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use Exception;

class ServiceController extends ApiController {

    public function getItems(Request $request, $governateid = null) {
        $validateAttributes = parent::validateHeadersOnly($request, 'GET', $this->requiredParams);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        $model = new Service();
        $model = $model->with('childern');
        $model = $model->where('parent_id', '0');
        if ($governateid != null)
            $model = $model->where('location', 'like', '%' . $governateid . '%');
        return parent::success($model->where('state', '1')->get());
    }

}
