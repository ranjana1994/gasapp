<?php

namespace App\Http\Controllers\API;

use App\Models\Order as MyModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use Exception;

class OrdersController extends ApiController {

    protected static function rules() {
        return ['user_customer_id' => 'required|exists:users,id', 'user_customer_address_id' => 'required|exists:user_addresses,id', 'service_id' => 'required|exists:services,id', 'service_child_id' => '', 'service_quantity' => '', 'service_price' => '', 'user_supplier_id' => 'required|exists:users,id'];
    }

    public function store(Request $request) {
        //Validating attributes
        $rules = self::rules();
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        try {
            $model = new MyModel();
            if (isset($request->service_child_id)):
                if (is_array(json_decode($request->service_child_id, true)))
                    $model->is_child = 1;
                else
                    return parent::error('Please Use valid syntax for adding service child id');
            endif;
            foreach (array_keys($rules) as $index):
                if ($index == 'service_child_id')
                    continue;
                else
                    $model->$index = $request->$index;
            endforeach;
            $model->state = '1';
            $model->save();
            if ($model->is_child == 1):
                foreach (json_decode($request->service_child_id, true) as $service_id => $service_quantity):
                    $modelChild = new \App\Models\OrderChild();
                    $modelChild->order_id = $model->id;
                    $modelChild->service_id = $service_id;
                    $modelChild->service_quantity = $service_quantity;
                    $modelChild->save();
                endforeach;
            endif;
            \App\Helpers\GasApplication::sendOrderNotification('customer', 'order_pending', $model);
            \App\Helpers\GasApplication::sendOrderNotification('supplier', 'order_pending', $model);
            return parent::successCreated('Created Successfully !!!');
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

    public function getItems(Request $request, $id, $serviceID = null) {
        $validateAttributes = parent::validateHeadersOnly($request, 'GET', $this->requiredParams);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        $model = MyModel::where('user_customer_id', $id)->with(['user_customer_address_id', 'user_customer_id', 'service_id', 'user_supplier_id', 'order_child'])->orderBy('id', 'desc');
        if ($serviceID !== null)
            $model = $model->where('service_id', $serviceID);
        return parent::success($model->where('state', '1')->get());
    }

    public function getOrderBySupplier(Request $request, $supplierId, $serviceID = null) {
        $validateAttributes = parent::validateHeadersOnly($request, 'GET', $this->requiredParams);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        $model = MyModel::where('user_supplier_id', $supplierId)->with(['user_customer_address_id', 'user_customer_id', 'service_id', 'user_supplier_id', 'order_child'])->orderBy('id', 'desc');
        if ($serviceID !== null)
            $model = $model->where('service_id', $serviceID);

        return parent::success($model->where('state', '1')->get());
    }

    public function updateStatusBySupplier(Request $request, $supplierId, $orderid) {
        //Validating attributes
        $rules = ['status' => 'required|in:canceled,processing,completed,rejected', 'review_supplier' => ''];
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        try {
            $model = MyModel::where('id', $orderid)->where('user_supplier_id', $supplierId);
            if ($model->get()->isEmpty())
                return parent::error('Order not found!');
            $model = MyModel::find($orderid);
            $model->request_status = $request->status;
            if ($request->review_supplier)
                $model->review_supplier = $request->review_supplier;
//            dd($model->get());
            $model->save();
            \App\Helpers\GasApplication::sendOrderNotification('customer', 'order_' . $request->status, $model);
            return parent::successCreated('Updated Successfully !!!');
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

    public function updateStatusByCustomer(Request $request, $customerId, $orderid) {
        //Validating attributes
        $rules = ['review_customer' => 'required|in:0,1,2,3,4,5'];
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        try {
            $model = MyModel::where('id', $orderid)->where('user_customer_id', $customerId);
            if ($model->get()->isEmpty())
                return parent::error('Order not found!');
            if ($model->first()->request_status != 'completed')
                return parent::successCreated('Order is not in completed state please check order status!', '203');
            $model = MyModel::find($orderid);
            $model->review_customer = $request->review_customer;
//            dd($model->get());
            $model->save();
            \App\Helpers\GasApplication::sendOrderNotification('supplier', 'order_' . $model->request_status, $model);
            return parent::successCreated('Updated Successfully !!!');
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($customerid, $orderid) {
        try {
            $modelcustomer = \App\User::where('id', $customerid)->role(\GasApplication::getUserRoles('customer', 'ctg'))->get();
            if ($modelcustomer->isEmpty() === true)
                return parent::error('Please use valid Customer');
            $modelorder = MyModel::where('id', $orderid)->where('user_customer_id', $customerid)->get();
            if ($modelorder->isEmpty() === true)
                return parent::error('Order not belongs to customer');
            $createdAt = $modelorder->first()->created_at;
            $now = \Carbon\Carbon::now()->toDateTimeString();
//            dd($now);
            $createdAt->diffInMinutes($now);
//            dd($createdAt->diffInMinutes($now));
            if ($createdAt->diffInMinutes($now) > 3)
                return parent::error('Time to delete order is finished please contact gas application department');
            if (MyModel::destroy($orderid)) {
                $data = 'Deleted Successfully';
            } else {
                return parent::error('Data corespondence to provided ID is not available');
            }
            return parent::successCreated($data);
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

}
