<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class AuthController extends ApiController {

    public function loginWithOTP(Request $request) {
        //Validating attributes
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, ['mobile_number' => 'required', 'password' => 'required']), ['mobile_number', 'password'], true);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        if (Auth::attempt(['mobile_number' => request('mobile_number'), 'password' => request('password')])):
            $checkUser = User::where('id', Auth::user()->id)->role(\GasApplication::getUserRoles('customer', 'ctg'))->get();
            if ($checkUser->isEmpty())
                return parent::error('User not found', 403);
            $user = User::find(Auth::user()->id);
            parent::sendOTP($user);
            $user = User::where('id', Auth::user()->id)->first();
            return parent::success(['message' => 'Please Check Your Mobile...', 'otp' => $user->otp]);
        else:
            return parent::error('User not found');
        endif;
    }

    public function resendOTP(Request $request) {
        //Validating attributes
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, ['mobile_number' => 'required']), ['mobile_number'], true);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        $user = User::where('mobile_number', $request->mobile_number)->get();
        if (count($user) > 0):
            $user = User::find($user->first()->id);
            parent::sendOTP($user);
            $user = User::where('id', $user->first()->id)->first();
            return parent::success(['message' => 'Please Check Your Mobile...', 'otp' => $user->otp]);
//            return parent::success(['message' => 'Please Check Your Mobile...']);
        else:
            return parent::error('User not found');
        endif;
    }

    public function verifyLogin(Request $request) {
        //Validating attributes
        $rules = ['mobile_number' => 'required', 'otp' => 'required', 'device_token' => 'required'];
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), true);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        $otp = $request->otp;
        if (strlen($otp) != '4')
            return parent::error('Please use valid pattern for OTP');
        try {
            $user = User::where('mobile_number', $request->mobile_number)->with('addresses')->first();
            if ($user === null)
                return parent::error('Please use valid User');
            Role::where('name', $user->getRoleNames()['0'])->first()->params;
            if ($user != null):
                if ($user->otp == $otp):
                    $user->otp = 0;
                    $user->device_token = $request->device_token;
                    $user->save();
                    return parent::success($user);
                else:
                    return parent::error('Please use valid OTP');
                endif;
            else:
                return parent::error('Please check user details provided.');
            endif;
        } catch (\Exception $ex) {
            return parent::error('Something went Wrong');
        }
    }

    public function registerCustomer(Request $request) {
        //Validating attributes
        $rules = \App\Http\Controllers\Admin\UserCustomerController::rules();
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        try {
            $model = new User();
            foreach (array_keys($rules) as $index):
                if ($index == 'role_customer')
                    continue;
                if ($index == 'password')
                    $model->$index = Hash::make($request->$index);
                else
                    $model->$index = $request->$index;
            endforeach;
            $model->assignRole($request->role_customer);
            $model->state = '1';
            $model->save();
            parent::sendOTP($model);

            $user = User::where('id', $model->id)->first();
            return parent::success(['message' => 'User created Succesfully, Please Check Your Mobile for verification process...', 'otp' => $user->otp]);
//            return parent::successCreated(['message' => 'User created Succesfully, Please Check Your Mobile for verification process...']);
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

    public function updateCustomer(Request $request, $id) {
        //Validating attributes
        $rules = \App\Http\Controllers\Admin\UserCustomerController::rules();
        unset($rules['mobile_number']);
        unset($rules['password']);
        unset($rules['email']);
//        dd($rules);
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        try {
            $model = User::find($id);
            foreach (array_keys($rules) as $index):
                if ($index == 'role_customer')
                    continue;
                if ($index == 'password')
                    $model->$index = Hash::make($request->$index);
                else
                    $model->$index = $request->$index;
            endforeach;
            $model->syncRoles($request->role_customer);
            $model->save();
            return parent::successCreated(['message' => 'User Updated Succesfully']);
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

    public function updateMobileNumber(Request $request, $id) {
        //Validating attributes
        $rules = ['mobile_number' => 'required|unique:users,mobile_number,' . $id];
//        dd($rules);
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        try {
            $model = User::find($id);
            if ($model->mobile_number == $request->mobile_number)
                return parent::success(['message' => 'Please use Different Number as it is your current number']);
            $model->mobile_number_tmp = $request->mobile_number;
            $model->save();
            $otpSent = parent::sendOTP($model, 'mobile_number_tmp');
//            if ($otpSent !== true)
//                return $otpSent;

            $user = User::where('id', $model->id)->first();
            return parent::success(['message' => 'Please Check Your Mobile...', 'otp' => $user->otp]);
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

    public function verifyMobileNumber(Request $request, $id) {
        //Validating attributes
        $rules = ['otp' => 'required'];
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), true);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        $otp = $request->otp;
        if (strlen($otp) != '4')
            return parent::error('Please use valid pattern for OTP');
        try {
            $user = User::find($id);
            if ($user != null):
                if ($user->otp == $otp):
                    $user->otp = 0;
                    $user->mobile_number = $user->mobile_number_tmp;
                    $user->mobile_number_tmp = 0;
                    $user->save();
                    return parent::success($user);
                else:
                    return parent::error('Please use valid OTP');
                endif;
            else:
                return parent::error('Please check user details provided.');
            endif;
        } catch (\Exception $ex) {
            return parent::error('Something went Wrong');
        }
    }

    public function registerSupplier(Request $request) {
        //Validating attributes
        $rulesSupplier = \App\Http\Controllers\Admin\UserSupplierController::rules();
        $rulesSupplier['role_supplier'] = 'required';
//        $rulesAddress = ['vehicle_number' => 'required|unique:user_suppliers,vehicle_number', 'license_number' => 'required|unique:user_suppliers,license_number', 'latitude' => '', 'longitude' => '', 'governorate_id' => '', 'city_id' => '', 'governorate_id' => 'required', 'city_id' => 'required', 'section_ids' => 'required'];
        $rulesAddress = ['latitude' => '', 'longitude' => '', 'governorate_id' => '', 'city_id' => '', 'governorate_id' => 'required', 'city_id' => 'required', 'section_ids' => 'required'];
        $rules = array_merge($rulesSupplier, $rulesAddress);
//        dd($rules);
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        try {

            $role = Role::where('id', $request->role_supplier)->get();
            if (count($role) === 0)
                return parent::error("Service You're looking for is not available", 401);
            $request->role_supplier = $role->first()->name;
//            dd($request->all());
            $model = new User();
            foreach (array_keys($rulesSupplier) as $index):
                if ($index == 'role_supplier')
                    continue;
                if ($index == 'password')
                    $model->$index = Hash::make($request->$index);
                else
                    $model->$index = $request->$index;
            endforeach;
            $model->assignRole($request->role_supplier);
            $model->state = '0';
            $model->save();
            //Add Supplier Address
            $modelSupplier = new \App\Models\UserSupplier();
            $modelSupplier->user_id = $model->id;
            foreach (array_keys($rulesAddress) as $index):
                $modelSupplier->$index = $request->$index;
            endforeach;
            $modelSupplier->state = '1';
            $modelSupplier->save();
            parent::sendOTP($model);
            $user = User::where('id', $model->id)->first();
            return parent::success(['message' => 'User created Succesfully, Please Check Your Mobile for verification process...', 'otp' => $user->otp]);
//            return parent::successCreated(['message' => 'Supplier created Succesfully, Please Check Your Mobile for verification process...']);
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

    public function sendOTPSupplier(Request $request, $serviceID) {
        //Validating attributes
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, ['mobile_number' => 'required', 'password' => 'required']), ['mobile_number', 'password'], true);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        $role = Role::where('id', $serviceID)->get();
        if (count($role) === 0)
            return parent::error("Service You're looking for is not available", 401);
//        dd($role->toArray());
//        $user = User::where('mobile_number', $request->mobile_number)->role($role->first())->get();
        if (Auth::attempt(['mobile_number' => request('mobile_number'), 'password' => request('password')])):
            $checkUser = User::where('id', Auth::user()->id)->role(\GasApplication::getUserRoles('supplier', 'ctg'))->get();
            if ($checkUser->isEmpty())
                return parent::error('User not found or not link with this service', 403);
            $user = User::find(Auth::user()->id);
            $otpSent = parent::sendOTP($user);
//            if ($otpSent !== true)
//                return $otpSent;
//            return parent::success(['message' => 'Please Check Your Mobile...']);

            $user = User::where('id', Auth::user()->id)->first();
            return parent::success(['message' => 'Please Check Your Mobile...', 'otp' => $user->otp]);
        else:
            return parent::error('User not found or not link with this service', 403);
        endif;
    }

    public function changeStatus(Request $request, $id) {
        $validateAttributes = parent::validateHeadersOnly($request, 'GET', $this->requiredParams);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        $model = User::find($id);
        if ($model === null)
            return parent::error('Please use valid User');
//        dd($model);
        $model->status = ($model->first()->status == '1' ? '0' : '1');
        $model->save();
        return parent::successCreated('Status Updated Successfully');
    }

    public function changePasswordWithAuth(Request $request) {
        //Validating attributes
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, ['user_id' => 'required', 'old_password' => 'required', 'password' => 'required|confirmed']), ['user_id', 'old_password', 'password', 'password_confirmation'], true);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        if (Auth::attempt(['id' => request('user_id'), 'password' => request('old_password')])):
//            dd(Auth::user()->id);
            $user = User::find(Auth::user()->id);
            $user->password = Hash::make($request->password);
            $user->save();
//            $otpSent = parent::sendOTP($user);
//            if ($otpSent !== true)
//                return $otpSent;
            return parent::successCreated(['message' => 'Password Changed Successfully...']);
        else:
            return parent::error("Provided credentials doesn't mattach...", 403);
        endif;
    }

    public function changePasswordWithOutAuth(Request $request) {
        //Validating attributes
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, ['user_id' => 'required', 'password' => 'required|confirmed']), ['user_id', 'password', 'password_confirmation'], true);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        $user = User::find($request->user_id);
        if ($user !== null):
//            dd(Auth::user()->id);
            $user->password = Hash::make($request->password);
            $user->save();
//            $otpSent = parent::sendOTP($user);
//            if ($otpSent !== true)
//                return $otpSent;
            return parent::successCreated(['message' => 'Password Changed Successfully...']);
        else:
            return parent::error("Provided User ID Not found...", 403);
        endif;
    }

}
