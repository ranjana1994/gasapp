<?php

namespace App\Http\Controllers\API;

use App\Models\UserSupplierServiceRate as MyModel;
use Illuminate\Http\Request;

class UserSupplierServiceRateController extends ApiController {

    protected static function rules() {
        return ['supplier_id' => 'required|exists:users,id', 'service_id' => 'required|exists:services,id', 'rate_cards' => 'required'];
    }

    public function getItemList(Request $request) {
        $rules = self::rules();
        unset($rules['rate_cards']);
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
//dd('s');
        $model = MyModel::where('supplier_id', $request->supplier_id)->where('service_id', $request->service_id);
        if (count($model))
            return parent::success($model->where('state', '1')->get());
        else
            return parent::error('No Data Found');
    }

    public function update(Request $request) {
        //Validating attributes
        $rules = self::rules();
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        try {
            $update = MyModel::where('supplier_id', $request->supplier_id)->get();
//        dd($rules);
            if ($update->isEmpty()):
                $model = new MyModel;
            else:
                $model = MyModel::find($update->first()->id);
            endif;
            foreach (array_keys($rules) as $index):
                $model->$index = $request->$index;
            endforeach;
            $model->state = '1';
            $model->save();
            return parent::success($model);
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

}
