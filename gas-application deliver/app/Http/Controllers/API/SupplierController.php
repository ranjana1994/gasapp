<?php

namespace App\Http\Controllers\API;

use App\User as MyModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use Exception;

class SupplierController extends ApiController {

    public function getItems(Request $request, $serviceroleid, $governateid, $mobilenumber = null) {
        $validateAttributes = parent::validateHeadersOnly($request, 'GET', $this->requiredParams);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
//        $servicename = \App\Models\Service::find($serviceroleid)->first()->name;
//        dd($servicename);
        $role = \Spatie\Permission\Models\Role::where('id', $serviceroleid)->where('guard_name', 'api')->first();
//        dd($role->name);
        if ($role === null)
            return parent::error('Please use valid Role ID');
        if (!in_array($role->name, \GasApplication::getUserRoles('supplier', 'ctg')))
            return parent::error('Please use valid Role ID corresponding to Supplier Role only');

        $model = MyModel::select('*')->role($role)->with(['supplierAddress', 'supplierRating', 'supplierRateCard']);
//        dd($model->toArray());
        if ($mobilenumber != null)
            $model = $model->where('mobile_number', 'like', $mobilenumber);
//        if ($governateid != null)
//            $model = $model->where('mobile_number', 'like', $governateid);

        $model = $model->where('status', '1');
//        dd($model->toArray());
        return parent::success($model->where('state', '1')->get());
    }

}
