<?php

namespace App\Http\Controllers\API;

use App\Models\UserSupplier as MyModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use Exception;

class UserSupplierController extends ApiController {

    protected static function rules() {
        return ['user_id' => 'required|exists:users,id', 'vehicle_number' => 'required|unique:user_suppliers,vehicle_number', 'license_number' => 'required|unique:user_suppliers,license_number', 'latitude' => '', 'longitude' => '', 'governorate_id' => '', 'city_id' => '', 'governorate_id' => '', 'city_id' => '', 'section_ids' => ''];
    }

    public function updateByID(Request $request, $id) {
        //Validating attributes
        $rules = self::rules();
        $rules['vehicle_number'] .= ',' . $id;
        $rules['license_number'] .= ',' . $id;
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        try {
            $model = MyModel::find($id);
            foreach (array_keys($rules) as $index):
                $model->$index = $request->$index;
            endforeach;
            $model->save();
            return parent::successCreated($model);
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

    public function update(Request $request) {
        //Validating attributes
        $rules = ['user_id' => 'required|exists:user_suppliers,user_id', 'latitude' => 'required', 'longitude' => 'required'];
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        try {
            $id = MyModel::where('user_id', $request->user_id)->first()->id;
            $model = MyModel::find($id);
            foreach (array_keys($rules) as $index):
                $model->$index = $request->$index;
            endforeach;
            $model->save();
            return parent::successCreated('Updated Succefully');
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

    public function store(Request $request) {
        //Validating attributes
        $rules = self::rules();
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
//        dd($rules);
        try {
            $model = new MyModel();
            foreach (array_keys($rules) as $index):
                $model->$index = $request->$index;
            endforeach;
            $model->state = '1';
            $model->save();
            return parent::successCreated($model);
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

    public function getItems(Request $request, $id) {
        $validateAttributes = parent::validateHeadersOnly($request, 'GET', $this->requiredParams);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        $model = MyModel::where('user_id', $id)->with(['governorate', 'city']);
        return parent::success($model->where('state', '1')->get());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            if (MyModel::destroy($id)) {
                $data = 'Deleted Successfully';
            } else {
                return parent::error('Data corespondence to provided ID is not available');
            }
            return parent::successCreated($data);
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

}
