<?php

namespace App\Http\Controllers\API;

use App\Models\UserSupplierFavourite as MyModal;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use Exception;
use Illuminate\Support\Facades\Hash;

class UserSupplierFavouriteController extends ApiController {

    public function getfavourite(Request $request, $supplierid, $customerid) {
        $validateAttributes = parent::validateHeadersOnly($request, 'GET', $this->requiredParams);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        $modelSupplier = \App\User::where('id', $supplierid)->role(\GasApplication::getUserRoles('supplier', 'ctg'))->get();
        if ($modelSupplier->isEmpty() === true)
            return parent::error('Please use valid supplierid');
        $modelCustomer = \App\User::where('id', $customerid)->role(\GasApplication::getUserRoles('customer', 'ctg'))->get();
        if ($modelCustomer->isEmpty() === true)
            return parent::error('Please use valid customerid');
        $checkFavourite = MyModal::where('supplier_id', $supplierid)->where('customer_id', $customerid)->get();
        if ($checkFavourite->isEmpty()):
            $favourite = new MyModal();
            $favourite->supplier_id = $supplierid;
            $favourite->customer_id = $customerid;
            $favourite->save();
            return parent::successCreated('Customer Marked as Favourite');
        else:
//            dd($checkFavourite->first()->toArray());
            MyModal::destroy($checkFavourite->first()->id);
            return parent::successCreated('Customer UnMarked as Favourite');
        endif;
    }

    public function getCustomersList(Request $request, $supplierId) {
        $validateAttributes = parent::validateHeadersOnly($request, 'GET', $this->requiredParams);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
//        dd($supplierId);

        $modelSupplier = \App\User::where('id', $supplierId)->role(\GasApplication::getUserRoles('supplier', 'ctg'))->get();
        if ($modelSupplier->isEmpty() === true)
            return parent::error('Please use valid supplierid');
        $modelSupplier = MyModal::where('supplier_id', $supplierId)->get();
        if ($modelSupplier->isEmpty() === true)
            return parent::successCreated('No Favourite Customer Found', '203');
        $drivers = MyModal::where('supplier_id', $supplierId)->get();
//         dd($drivers->pluck('customer_id')->toArray());
        $model = \App\User::wherein('id', $drivers->pluck('customer_id')->toArray());
//        dd($selected);

        if (count($model))
            return parent::success($model->where('state', '1')->get());
        else
            return parent::error('No Data Found');
    }

}

?>