<?php

namespace App\Http\Controllers\API;

use App\Models\UserDriver;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use Exception;
use Illuminate\Support\Facades\Hash;

class UserDriverController extends ApiController {

    public function getDriverList(Request $request, $supplierId) {
        $validateAttributes = parent::validateHeadersOnly($request, 'GET', $this->requiredParams);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
//        dd($supplierId);
        $drivers = \App\Models\UserDriver::where('parent_user_id', $supplierId)->get();
//            dd($drivers->pluck('user_id')->toArray());
        $model = \App\User::select('*')->wherein('id', $drivers->pluck('user_id')->toArray())->role(\GasApplication::getUserRoles('12'))->with('driver');
//        dd($selected);
        if (count($model))
            return parent::success($model->where('state', '1')->get());
        else
            return parent::error('No Data Found');
    }

    public function getDriverCreate(Request $request, $supplierId) {
        $rules = \App\Http\Controllers\Admin\UserDriverController::rules();
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
//Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        try {
            $model = new \App\User();
            if ($model == null)
                return parent::error('Supplier not found');
            foreach (array_keys($rules) as $index):
                if (strpos($index, 'driver_') !== false)
                    continue;
                if ($index == 'password')
                    $model->$index = Hash::make($request->$index);
                else
                    $model->$index = $request->$index;
            endforeach;
            $model->assignRole(\GasApplication::getUserRoles('12'));
            $model->state = '1';
            $model->save();
            $driver = new \App\Models\UserDriver();
            $driver->user_id = $model->id;
            $driver->parent_user_id = $supplierId;
            $driver->vehicle_number = $request->driver_vehicle_number;
            $driver->license_number = $request->driver_license_number;
            $driver->save();
            return parent::successCreated('Created Successfully !!!');
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

    public function update(Request $request, $driverId) {
        $rules = \App\Http\Controllers\Admin\UserDriverController::rules();
        $rules['mobile_number'] .= ',' . $driverId;
        $rules['driver_vehicle_number'] .= ',' . $driverId.',user_id';
        $rules['driver_license_number'] .= ',' . $driverId.',user_id';
//        dd($rules);
        $validateAttributes = parent::validateAttributes($request, 'POST', array_merge($this->requiredParams, $rules), array_keys($rules), false);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
//Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;


        try {
            $model = \App\User::find($driverId);
            if ($model == null)
                return parent::error('Driver not found');
            foreach (array_keys($rules) as $index):
                if (strpos($index, 'driver_') !== false)
                    continue;
                if ($index == 'password')
                    $model->$index = Hash::make($request->$index);
                else
                    $model->$index = $request->$index;
            endforeach;
            $model->save();
            $driverM = \App\Models\UserDriver::where('user_id', $driverId)->get();
            $driver = UserDriver::find($driverM->first()->id);
            $driver->vehicle_number = $request->driver_vehicle_number;
            $driver->license_number = $request->driver_license_number;
            $driver->save();
            return parent::successCreated('Updated Successfully !!!');
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

    public function destroy($driverId) {
        try {
            $model = \App\User::find($driverId);
            if ($model == null)
                return parent::error('Supplier not found');
            if (\App\User::destroy($driverId)) {
                \App\Models\UserDriver::destroy($driverId);
                $data = 'Deleted Successfully';
            } else {
                return parent::error('Data corespondence to provided ID is not available');
            }
            return parent::successCreated($data);
        } catch (\Exception $ex) {
            return parent::error($ex->getMessage());
        }
    }

}

?>