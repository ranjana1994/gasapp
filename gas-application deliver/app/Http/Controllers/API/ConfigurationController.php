<?php

namespace App\Http\Controllers\API;

use App\Models\Configuration;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use Exception;

class ConfigurationController extends ApiController {

    public function getItem(Request $request, $type) {
        //Validating attributes
        $validateAttributes = parent::validateHeadersOnly($request, 'GET', $this->requiredParams);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        if (!in_array($type, ['tnc', 'howto','ads'])):
            return self::error('Please use valid type.', 422);
        endif;
        $model = Configuration::first();
//        dd($model);
        $type = $type . (self::$locale != '' ? '_' . self::$locale : '');
        if (isset($model->id))
            return parent::success(['item' => $model->$type]);
        else
            return parent::error(['message' => 'No Data Found']);
    }

}
