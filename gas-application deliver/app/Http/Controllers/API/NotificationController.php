<?php

namespace App\Http\Controllers\API;

use App\Models\Location;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use Exception;

class NotificationController extends ApiController {

    public function getItems(Request $request, $type, $user_id) {
        //Validating attributes
        $validateAttributes = parent::validateHeadersOnly($request, 'GET', $this->requiredParams);
        if ($validateAttributes):
            return $validateAttributes;
        endif;
        //Validating Client Details
        $validateClientSecret = parent::validateClientSecret();
        if ($validateClientSecret):
            return $validateClientSecret;
        endif;
        if (!in_array($type, ['supplier', 'customer']))
            return self::error('Please use valid type of user.', 422);
        $model = \App\Models\GasNotification::where('user_type', $type)->where('user_id', $user_id)->orderBy('id','desc');
        if (count($model))
            return parent::success($model->get());
        else
            return parent::error('No Data Found');
    }

}
