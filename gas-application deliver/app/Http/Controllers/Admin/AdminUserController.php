<?php

namespace App\Http\Controllers\Admin;

use App\AdminUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use Auth;

class AdminUserController extends \App\Http\Controllers\AdminController {

    public static function rules() {
        return ['name' => 'required|max:100', 'email' => 'required|max:255', 'password' => 'required', 'role' => 'required|in:' . implode(',', \Spatie\Permission\Models\Role::where('guard_name', 'web-admin')->pluck('name')->toArray())];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/adminuser';
    }

    protected $baseVIEW = 'admin.adminuser';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $selected = AdminUser::select('*');
            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->editColumn('updated_at', function ($data) {
                                return $data->updated_at->toDayDateTimeString();
                            })
                            ->addColumn('action', function($selected) {
                                return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>';
                            })
                            ->rawColumns(['action'])->make(true);
        }
        return view($this->baseVIEW . '.index', ['title' => 'Admin User', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['name', 'email']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        return view($this->baseVIEW . '.form', ['title' => 'Create Admin User', 'action' => url(self::baseURL()), 'actionMessage' => 'Admin User Created Successfully', 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make(Input::all(), self::rules());
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = new AdminUser();
                foreach (array_keys(self::rules()) as $index):
                    if ($index == 'role')
                        continue;
                    if ($index == 'password')
                        $model->$index = Hash::make($request->$index);
                    else
                        $model->$index = $request->$index;
                endforeach;
                $model->assignRole($request->role);
//                $model->state = '1';
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminUser  $role
     * @return \Illuminate\Http\Response
     */
    public function show(AdminUser $role) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminUser  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = AdminUser::find($id);
        $data->password = '';
        return view($this->baseVIEW . '.form', ['title' => 'Edit Admin User', 'action' => url(self::baseURL()) . '/' . $id, 'actionMessage' => 'Admin User Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminUser  $role
     * @return \Illuminate\Http\Response
     */
    public function changeState(Request $request, $id) {
        if ($model = AdminUser::find($id)) {
            $model->state = $request->checked == 'true' ? '1' : '0';
            $model->save();
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminUser  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validator = Validator::make(Input::all(), self::rules());
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = AdminUser::find($id);
                foreach (array_keys(self::rules()) as $index):
                    if ($index == 'role')
                        continue;
                    if ($index == 'password')
                        $model->$index = Hash::make($request->$index);
                    else
                        $model->$index = $request->$index;
                endforeach;
                $model->syncRoles($request->role);
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminUser  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (AdminUser::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

}
