<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use Auth;

class NotificationController extends \App\Http\Controllers\AdminController {

    public static function rules() {
        return ['title' => 'required', 'body' => 'required'];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/notification';
    }

    protected $baseVIEW = 'admin.notification';

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function notify(Request $request, $type, $target) {
        $validator = Validator::make(Input::all(), self::rules());
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                switch ($type):
                    case'customer':
                        $user = User::find($target)->first();
                        \App\Http\Controllers\API\ApiController::pushNotification(['title' => $request->title, 'body' => $request->body], $user->device_token);
                        break;
                    case'role':

                        $user = User::select('*')->role($target)->get();
                        \App\Http\Controllers\API\ApiController::pushNotification(['title' => $request->title, 'body' => $request->body], $user->pluck('device_token')->toArray());
                        break;
                    case'governorate':
                        $userAddress = \App\Models\UserAddress::where('governorate_id', $target)->get();
                        $user = User::whereIn('id',$userAddress->pluck('user_id'))->get();
                        \App\Http\Controllers\API\ApiController::pushNotification(['title' => $request->title, 'body' => $request->body], $user->pluck('device_token')->toArray());
                        break;
                    case'location':
                        break;
                endswitch;
//                dd($request->all());
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

}
