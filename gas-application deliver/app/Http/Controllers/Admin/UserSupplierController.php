<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use Auth;

class UserSupplierController extends \App\Http\Controllers\AdminController {

    public static function rules() {
        return ['name' => 'required|max:100', 'last_name' => 'required|max:100', 'email' => '', 'mobile_number' => 'required|unique:users,mobile_number', 'password' => '', 'role_supplier' => 'required|in:' . implode(',', \GasApplication::getUserRoles('supplier', 'ctg'))];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/suppliers';
    }

    protected $baseVIEW = 'admin.supplier';

    public function index() {
//        dd(self::rules());
        if (request()->ajax()) {
            $selected = User::select('*')->role(\GasApplication::getUserRoles('supplier', 'ctg'));
            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->editColumn('updated_at', function ($data) {
                                return $data->updated_at->toDayDateTimeString();
                            })
                            ->addColumn('action', function($selected) {
                                return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/">View</a><a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>';
                            })
                            ->addColumn('status', function($selected) {
                                $checkbox = ($selected->state == '1') ? 'checked' : '';
                                return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                            })
                            ->rawColumns(['action', 'status', 'status'])->make(true);
        }
        return view($this->baseVIEW . '.index', ['title' => 'Supplier', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['name', 'mobile_number', 'status']]);
    }

    public function indexSearch(Request $request) {
//        dd(self::rules());
        if (request()->ajax()) {
            if (count($request->search['value']))
                $selected = User::select('*')->role(\GasApplication::getUserRoles('supplier', 'ctg'));
            else
                $selected = [];
            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->editColumn('updated_at', function ($data) {
                                return $data->updated_at->toDayDateTimeString();
                            })
                            ->addColumn('action', function($selected) {
                                return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/">View</a><a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>';
                            })
                            ->addColumn('status', function($selected) {
                                $checkbox = ($selected->state == '1') ? 'checked' : '';
                                return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                            })
                            ->rawColumns(['action', 'status', 'status'])->make(true);
        }
        return view($this->baseVIEW . '.index', ['title' => 'Supplier', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['name', 'mobile_number', 'status']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view($this->baseVIEW . '.form', ['title' => 'Create Supplier', 'action' => url(self::baseURL()), 'actionMessage' => 'Supplier Created Successfully', 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = self::rules();
        $rules['role_supplier'] = 'required';
//        dd($rules);
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = new User();
                foreach (array_keys(self::rules()) as $key):
                    $index = str_replace('edit_', '', $key);
                    if ($index == 'role_supplier')
                        continue;
                    if ($index == 'password')
                        $model->$index = Hash::make($request->$key);
                    else
                        $model->$index = $request->$key;
                endforeach;
//                dd($request->role_supplier);
                $model->assignRole($request->role_supplier);
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request) {
        $data = User::where('id', $id)->first();
        return view($this->baseVIEW . '.view', ['title' => 'Supplier', 'actionMessage' => 'Supplier List', 'data' => $data, 'listUrl' => url(self::baseURL()), 'editUrl' => url(self::baseURL()) . '/' . $id . '/edit', 'driver_fields' => ['name', 'mobile_number', 'status']]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = User::find($id);
        $data->password = '';
        return view($this->baseVIEW . '.form', ['title' => 'Edit Supplier', 'action' => url(self::baseURL()) . '/' . $id, 'actionMessage' => 'Supplier Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function changeState(Request $request, $id) {
        if ($model = User::find($id)) {
            $model->state = $request->checked == 'true' ? '1' : '0';
            $model->save();
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $rules = self::rules();
        $rules['role_supplier'] = 'required';
        $rules['mobile_number'] .= ',' . $id;
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = User::find($id);
                foreach (array_keys(self::rules()) as $key):
                    $index = str_replace('edit_', '', $key);
                    if ($index == 'role_supplier')
                        continue;
                    if ($index == 'password')
                        $model->$index = Hash::make($request->$key);
                    else
                        $model->$index = $request->$key;
                endforeach;
                $model->syncRoles($request->role_supplier);
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (User::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

}
