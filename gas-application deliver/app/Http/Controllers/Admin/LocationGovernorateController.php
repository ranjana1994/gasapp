<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\Location;
use Auth;

class LocationGovernorateController extends \App\Http\Controllers\AdminController {

    protected static function rules() {
        return ['name' => 'required|max:100', 'name_kr' => '', 'name_ar' => '', 'code' => ''];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/governorate';
    }

    protected $baseVIEW = 'admin.location.governorate';

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $role
     * @return \Illuminate\Http\Response
     */
    public function importFileIntoDB(Request $request) {
        //try {
        if ($request->hasFile('sample_file')) {
            $path = $request->file('sample_file')->getRealPath();
            $data = \Excel::load($path)->get();
            if ($data->count()) {
//                dd($data);
//                dd($data->toArray());
                $location = new Location();
                $i = 0;
                foreach ($data->toArray() as $key => $value) {
                    $i++;
                    $governorate = $location->where('name', $value['governorate'])->get();
                    if ($governorate->count() == 0):
                        $governorate = $location->insertGetId(['name' => $value['governorate'], 'name_kr' => $value['governorate_kr'], 'name_ar' => $value['governorate_ar'], 'code' => $value['governorate_code'], 'type' => 'governorate']);
                    else:
                        $governorate = $governorate->first()->id;
                    endif;
                    $city = $location->where('name_kr', $value['city_kr'])->get();
                    if ($city->count() == 0):
                        $city = $location->insertGetId(['name' => $value['city'], 'name_kr' => $value['city_kr'], 'name_ar' => $value['city_ar'], 'code' => $value['city_code'], 'type' => 'city', 'parent_id' => $governorate]);
                    else:
                        $city = $city->first()->id;
                    endif;
                    $section = $location->where('name_kr', $value['section_kr'])->get();
                    if ($section->count() == 0):
                        $location->insertGetId(['name' => $value['section'], 'name_kr' => $value['section_kr'], 'name_ar' => $value['section_ar'], 'code' => $value['section_code'], 'type' => 'section', 'parent_id' => $city]);
                    endif;
                }
                return \redirect(self::baseURL())->with(["message" => $i . ' Location added']);
            }
        }
        //} catch (\Exception $ex) {
        return \redirect()->back()->withErrors(['error' => 'Something Went Wrong!']);
        // }
        return \redirect()->back()->withErrors(['error' => 'File is required!']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $selected = Location::select('*')->where('type', 'governorate');
            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->editColumn('updated_at', function ($data) {
                                return $data->updated_at->toDayDateTimeString();
                            })
                            ->addColumn('action', function($selected) {
                                return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>'
                                        . '<a class = "ajaxify btn btn-info btn-sm notify" href = "' . url(config('app.admin_prefix') . '/notification/governorate/' . $selected->id) . '">Notify</a>';
                            })
                            ->addColumn('status', function($selected) {
                                $checkbox = ($selected->state == '1') ? 'checked' : '';
                                return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                            })
                            ->addColumn('checkbox', function ($selected) {
                                return '<input class="delete-state" type="checkbox" id="' . $selected->id . '" name="chk">';
                            })
                            ->rawColumns(['action', 'status', 'checkbox'])->make(true);
        }
        return view($this->baseVIEW . '.index', ['title' => 'Location Governorate', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['name', 'code', 'status']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        return view($this->baseVIEW . '.form', ['title' => 'Create Location Governorate', 'action' => url(self::baseURL()), 'actionMessage' => 'Location Governorate Created Successfully', 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make(Input::all(), self::rules());
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = new Location();
                foreach (array_keys(self::rules()) as $key):
                    $model->$key = $request->$key;
                endforeach;
                $model->type = 'governorate';
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Location $role) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = Location::find($id);
        $data->password = '';
        return view($this->baseVIEW . '.form', ['title' => 'Edit Location Governorate', 'action' => url(self::baseURL()) . '/' . $id, 'actionMessage' => 'Location Governorate Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $role
     * @return \Illuminate\Http\Response
     */
    public function changeState(Request $request, $id) {
        if ($model = Location::find($id)) {
            $state = $request->checked == 'true' ? '1' : '0';
            $model->state = $state;
            $model->save();
            $cities = Location::where('parent_id', $id)->get();
            foreach ($cities as $city):
                $city->state = $state;
                $city->save();
                $sections = Location::where('parent_id', $city->id)->get();
                foreach ($sections as $section):
                    $section->state = $state;
                    $section->save();
                endforeach;
            endforeach;
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $Validator = Validator::make(Input::all(), self::rules());
        if ($Validator->fails()) {
            return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = Location::find($id);
                foreach (array_keys(self::rules()) as $key):
                    $model->$key = $request->$key;
                endforeach;
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Location::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

    public function multipledestroy(Request $request) {
        try {
            foreach ($request->checked as $id):
                Location::destroy($id);
            endforeach;
            $data = 'Success';
        } catch (Exception $ex) {
            $data = 'something went wrong';
        }
        return response()->json($data);
    }

}
