<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Validator;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;

class UserController extends \App\Http\Controllers\AdminController {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customerimportFileIntoDB(Request $request) {
        try {
            if ($request->hasFile('sample_file')) {
                $path = $request->file('sample_file')->getRealPath();
                $data = \Excel::load($path)->get();
                if ($data->count()) {
//                    dd($data);
//                    dd($data->toArray());
                    $i = 0;
                    foreach ($data->toArray() as $key => $value) {
                        $i++;
                        try {
                            $model = new User();
                            foreach (array_keys(UserCustomerController::rules()) as $index):
                                if ($index == 'role_customer')
                                    continue;
                                if ($index == 'password')
                                    $model->$index = Hash::make($value[$index]);
                                else
                                    $model->$index = $value[$index];
                            endforeach;
                            $model->assignRole($value['role_customer']);
                            $model->save();
                        } catch (\Exception $ex) {
                            continue;
                        }
                    }
                    return \redirect()->back()->with(["message" => $i . ' customer added']);
                }
            }
        } catch (\Exception $ex) {
            return \redirect()->back()->withErrors(['error' => 'Something Went Wrong!']);
        }
        return \redirect()->back()->withErrors(['error' => 'File is required!']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function supplierimportFileIntoDB(Request $request) {
        try {
            if ($request->hasFile('sample_file')) {
                $path = $request->file('sample_file')->getRealPath();
                $data = \Excel::load($path)->get();
                if ($data->count()) {
                //  dd($data);
//                    dd($data->toArray());
                    $i = 0;
                    foreach ($data->toArray() as $key => $value) {
                       
                        $i++;
                        try {
                            $model = new User();
                            foreach (array_keys(UserSupplierController::rules()) as $index):
                                if ($index == 'role_supplier')
                                    continue;
                                if ($index == 'password')
                                    $model->$index = Hash::make($value[$index]);
                                else
                                    $model->$index = $value[$index];
                            endforeach;
//                            dd(explode(',', $value['role_supplier']));
                           $model->assignRole(explode(',', $value['role_supplier']));
//                            dd($model);
                            $model->save();
                        } catch (\Exception $ex) {
                            continue;
                        }
                    }
                    return \redirect()->back()->with(["message" => $i . ' supplier added']);
                }
            }
        } catch (\Exception $ex) {
            return \redirect()->back()->withErrors(['error' => 'Something Went Wrong!']);
        }
        return \redirect()->back()->withErrors(['error' => 'File is required!']);
    }
    
    
      public function index() {
        if (request()->ajax()) {
            $selected = User::select('id', 'name', 'email', 'created_at', 'updated_at');
            return Datatables::of($selected)
                            ->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->addColumn('role', function($selected) {
                                return $selected->getRoleNames();
                            })->addColumn('action', function($selected) {
                                return
                                        '<button type="button" class="btn btn-info btn-sm btnEdit" data-edit="' . url('/admin') . '/users/' . $selected->id . '/edit">Edit</button>
                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url('/admin') . '/users/' . $selected->id . '">Delete</button>';
                            })
                            ->make(true);
        }
        $roles = Role::all();
        return view('admin.users', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'name' => 'required|min:2|max:32',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|max:8',
            'role' => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {

            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $role = $request->role;
            $user->save();
            $user->assignRole($role);

            return response()->json(array("success" => true));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) {
//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = User::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $rules = [
            'edit_name' => 'required|min:2|max:32',
            'edit_email' => 'required|email',
//            'edit_password' => 'required|min:6|max:8',
            'edit_role' => 'required'
        ];

        $message = [
            'edit_name.required' => 'The Name field is required.',
            'edit_name.min' => 'The Name must be at least 2 characters.',
            'edit_name.max' => 'The Name may not be greater than 32 characters.',
            'edit_email.required' => 'The email field is required.',
            'edit_email.email' => 'must be email',
            'edit_password.required' => 'The Password field is required.',
            'edit_password.min' => 'The Password must be at least 5 characters.',
            'edit_password.max' => 'The Password may not be greater than 100 characters.',
        ];

        $Validator = Validator::make(Input::all(), $rules, $message);

        if ($Validator->fails()) {
            return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
        } else {
            $user = User::find($id);
            $user->name = $request->edit_name;
            $user->email = $request->edit_email;
            $user->password = bcrypt($request->edit_password);
            $role = $request->edit_role;
            $user->save();
            $user->syncRoles($role);

            return response()->json(array("success" => true));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (User::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function changeState(Request $request, $id) {
        if ($model = User::find($id)) {
            $model->state = $request->checked == 'true' ? '1' : '0';
            $model->save();
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

}
