<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\Service;
use Auth;

class ServiceController extends \App\Http\Controllers\AdminController {

    protected static function rules() {
//        dd(\Spatie\Permission\Models\Role::where('guard_name', 'api')->pluck('id')->toArray());
        return ['image' => 'mimes:jpeg,jpg,png', 'name' => 'required', 'image_kr' => 'mimes:jpeg,jpg,png', 'name_kr' => '', 'image_ar' => 'mimes:jpeg,jpg,png', 'name_ar' => '', 'location' => 'required', 'unit' => 'required|in:liter,quantity,set,barrel', 'parent_id' => 'required','default_qty' => '', 'role_id' => 'required|in:' . implode(',',\Spatie\Permission\Models\Role::where('guard_name', 'api')->pluck('id')->toArray())];
//        return ['image' => 'mimes:jpeg,jpg,png', 'name' => 'required'];
    }

    protected $media = ['image', 'image_kr', 'image_ar'];

    protected static function baseURL() {
        return config('app.admin_prefix') . '/services';
    }

    protected $baseVIEW = 'admin.service';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $selected = Service::select('*');
            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->editColumn('updated_at', function ($data) {
                                return $data->updated_at->toDayDateTimeString();
                            })
                            ->addColumn('action', function($selected) {
                                return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>';
                            })
                            ->addColumn('status', function($selected) {
                                $checkbox = ($selected->state == '1') ? 'checked' : '';
                                return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                            })
                            ->rawColumns(['action', 'status'])->make(true);
        }
        return view($this->baseVIEW . '.index', ['title' => 'Service', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['name', 'name_kr', 'name_ar', 'status']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view($this->baseVIEW . '.form', ['title' => 'Create Service', 'action' => url(self::baseURL()), 'actionMessage' => 'Service Created Successfully', 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make(Input::all(), self::rules());
        if ($validator->fails()) {
            return \redirect()->back()->withErrors($validator->getMessageBag()->toArray())->withInput($request->except('image'));
        } else {
            try {
                $model = new Service();
                foreach ($this->media as $file):
                    if ($request->$file):
                        $photoName = time() . '.' . $request->$file->getClientOriginalExtension();
                        $request->$file->move(public_path('uploads/services'), $photoName);
                        $model->$file = $photoName;
                    endif;
                endforeach;
                foreach (array_keys(self::rules()) as $key):
                    if (in_array($key, $this->media))
                        continue;
                    if ($key == 'location')
                        $model->$key = json_encode($request->$key);
                    else
                        $model->$key = $request->$key;
                endforeach;
                $model->save();
                return \redirect(self::baseURL())->with(["success" => true]);
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Service $role) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = Service::find($id);
        $data->password = '';
        return view($this->baseVIEW . '.form', ['title' => 'Edit Service', 'action' => url(self::baseURL()) . '/' . $id, 'actionMessage' => 'Service Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $role
     * @return \Illuminate\Http\Response
     */
    public function changeState(Request $request, $id) {
        if ($model = Service::find($id)) {
            $model->state = $request->checked == 'true' ? '1' : '0';
            $model->save();
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

    public function update(Request $request, $id) {
//                dd($request->all());
        $validator = Validator::make(Input::all(), self::rules());
        if ($validator->fails()) {
            return \redirect()->back()->withErrors($validator->getMessageBag()->toArray())->withInput($request->except('image'));
        } else {
            try {
//                dd($request->all());
                $model = Service::find($id);
                foreach ($this->media as $file):
                    if ($request->$file):
                        $photoName = time() . '.' . $request->$file->getClientOriginalExtension();
                        $request->$file->move(public_path('uploads/services'), $photoName);
                        $model->$file = $photoName;
                    endif;
                endforeach;
                foreach (array_keys(self::rules()) as $key):
                    if (in_array($key, $this->media))
                        continue;
                    if ($key == 'location')
                        $model->$key = json_encode($request->$key);
                    else
                        $model->$key = $request->$key;
                endforeach;
//                dd($model);
                $model->save();
                return \redirect(self::baseURL())->with(["success" => true]);
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Service::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

}
