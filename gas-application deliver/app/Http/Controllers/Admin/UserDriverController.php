<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use Auth;

class UserDriverController extends \App\Http\Controllers\AdminController {

    public static function rules() {
        return ['name' => 'required|max:100', 'email' => '', 'mobile_number' => 'required|unique:users,mobile_number', 'password' => '', 'driver_vehicle_number' => 'required|unique:user_drivers,vehicle_number', 'driver_license_number' => 'required|unique:user_drivers,license_number'];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/drivers';
    }

    protected $baseVIEW = 'admin.driver';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $url = explode('/', url()->previous());
            $supplier = end($url);
            $drivers = \App\Models\UserDriver::where('parent_user_id', $supplier)->get();
//            dd($drivers->pluck('user_id')->toArray());
            $selected = User::select('*')->wherein('id', $drivers->pluck('user_id')->toArray())->role(\GasApplication::getUserRoles('12'));
            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                        return $data->created_at->toDayDateTimeString();
                    })->editColumn('updated_at', function ($data) {
                        return $data->updated_at->toDayDateTimeString();
                    })->addColumn('action', function($selected) {
                        return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>';
                    })->addColumn('status', function($selected) {
                        $checkbox = ($selected->state == '1') ? 'checked' : '';
                        return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                    })->rawColumns(['action', 'status'])->make(true);
        }
        return view($this->baseVIEW . '.index', ['title' => 'Driver', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['name', 'mobile_number','status']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $url = explode('/', url()->previous());
        $supplier = end($url);
        return view($this->baseVIEW . '.form', ['title' => 'Create Driver', 'action' => url(self::baseURL()), 'actionMessage' => 'Driver Created Successfully', 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules(), 'supplier' => $supplier]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = self::rules();
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = new User();
                foreach (array_keys($rules) as $key):
                    $index = str_replace('edit_', '', $key);
                    if (strpos($index, 'driver_') !== false)
                        continue;
                    if ($index == 'password')
                        $model->$index = Hash::make($request->$key);
                    else
                        $model->$index = $request->$key;
                endforeach;
                $model->assignRole(\GasApplication::getUserRoles('12'));
                $model->save();
                $driver = new \App\Models\UserDriver();
                $driver->user_id = $model->id;
                $driver->parent_user_id = $request->parent_user_id;
                $driver->vehicle_number = $request->driver_vehicle_number;
                $driver->license_number = $request->driver_license_number;
                $driver->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function show(User $role) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = User::where('id', $id)->with('driver')->first();
        $data->password = '';
        $data->driver_vehicle_number = $data->driver->vehicle_number;
        $data->driver_license_number = $data->driver->license_number;
        $url = explode('/', url()->previous());
        $supplier = end($url);
        return view($this->baseVIEW . '.form', ['title' => 'Edit Driver', 'action' => url(self::baseURL()) . '/' . $id, 'actionMessage' => 'Driver Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules(), 'supplier' => $supplier]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function changeState(Request $request, $id) {
        if ($model = User::find($id)) {
            $model->state = $request->checked == 'true' ? '1' : '0';
            $model->save();
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id ,$driverId) {
        $rules = self::rules();
        $rules['mobile_number'] .= ','.$id;
        $rules['driver_vehicle_number'] .= ','.$id;
        $rules['driver_license_number'] .= ','.$id;
        
        $Validator = Validator::make(Input::all(), $rules);
        if ($Validator->fails()) {
            return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = User::find($id);
                foreach (array_keys($rules) as $key):
                    $index = str_replace('edit_', '', $key);
                    if (strpos($index, 'driver_') !== false)
                        continue;
                    if ($index == 'password')
                        $model->$index = Hash::make($request->$key);
                    else
                        $model->$index = $request->$key;
                endforeach;
                $model->save();

                $driver = \App\Models\UserDriver::where('user_id', $id)->first();

//                dd($driver->first()->toArray());
                $driver->vehicle_number = $request->driver_vehicle_number;
                $driver->license_number = $request->driver_license_number;
                $driver->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (User::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

}
