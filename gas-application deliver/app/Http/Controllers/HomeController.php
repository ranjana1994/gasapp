<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;

class HomeController extends Controller {

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware(function ($request, $next) {
            //dd(\Auth::user());
            if (\Auth::user() === null)
                return redirect('/login');
            return $next($request);
        });
       
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard() {
        $services = \App\Models\Service::where('state', '1')->where('parent_id', '0')->with('childern')->get();
//         dd($services->toArray());
        return view('frontend.home', compact('services'));
    }

    public function getservice() {

        return view('frontend.order');
    }

    public function profile() {
        $users = \Auth::user();
        return view('frontend.profile', ['users' => $users]);
    }

    public function profilesave(Request $request) {
        $rules = Admin\UserCustomerController::rules();
        unset($rules['role_customer']);
        unset($rules['password']);
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = \App\User::find(\Auth::user()->id);
                foreach (array_keys($rules) as $index):
                    if ($index == 'role_customer')
                        continue;
                    if ($index == 'password')
                        $model->$index = Hash::make($request->$index);
                    else
                        $model->$index = $request->$index;
                endforeach;
                $model->save();
                 return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    public function orderquantity() {
        return view('frontend.order-quantity');
    }

//    public function ordergas() {
//        $currentuser = \App\User::where('id', \Auth::user()->id)->with('addresses')->first();
//        return view('frontend.order-gas', compact('currentuser'));
//    }

    public function getphonenumber(Request $request) {
        $data = \App\User::where('mobile_number', 'like', '%' . $request->search . '%')->get();
//        dd(count($data));
        return (count($data) == 0) ? ['errors' => 'Supplier Not Found with this Mobile Number'] : $data->first();
    }

    public function ordershow($id) {
        $orderlist = \App\Models\Service::findOrFail($id);
        $message = trans('passwords.type');
        //dd($orderlist);
        $currentuser = \App\User::where('id', \Auth::user()->id)->with('addresses')->first();
        return view('frontend.order-gas', compact('orderlist', 'currentuser','message'));
    }

}
