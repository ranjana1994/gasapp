<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;

class UserLoginController extends Controller {

    public function __construct() {
//        $this->middleware('guest', ['except' => 'logout']);
    }

    public function Login() {
        if (\Auth::user())
            return redirect('dashboard');
        else
            return view('frontend.login');
    }

    public function Auth(Request $request) {
        $validator = Validator::make(Input::all(), ['mobile_number' => 'required', 'password' => 'required']);
        if ($validator->fails())
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));

        if (\Auth::attempt(['mobile_number' => request('mobile_number'), 'password' => request('password')])) {
            Auth::login(\Auth::user(), true);
          //$user = \Auth::user();
           
            return response()->json(array("success" => true));
        } else {
            return response()->json(["errors" => ['mobile_number'=>'User not found']]);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {

        \Auth::logout();
        return redirect('/');
    }

}
