<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */


$adminPrefix = config('app.admin_prefix');
if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    // Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}

Route::any('/', function()use($adminPrefix) {
    return redirect('/login');
});
Route::any($adminPrefix, function()use($adminPrefix) {
    return redirect($adminPrefix . '/login');
});
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web']], function () {
    '\vendor\uniSharp\LaravelFilemanager\Lfm::routes()';
});
/* * ** Admin Routes **** */
Route::group(['middleware' => ['web']], function ()use($adminPrefix) {
//    Route::get('login', 'UserLoginController@getUserLogin');
//    Route::post('login', ['as' => 'user.auth', 'uses' => 'UserLoginController@userAuth']);
    Route::get($adminPrefix . '/delete-all/{model}', 'AdminController@deleteAll');
    Route::get($adminPrefix . '/login', 'AdminLoginController@getAdminLogin')->name('admin.login');
    Route::post($adminPrefix . '/login', ['as' => 'admin.auth', 'uses' => 'AdminLoginController@adminAuth']);
    Route::get($adminPrefix . '/password-reset', ['as' => 'admin.reset', 'uses' => 'Auth\PasswordController@resetPassoword']);

    // Password reset routes...
    Route::get($adminPrefix . '/password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post($adminPrefix . '/password/reset', 'Auth\PasswordController@postReset')->name('password.reset');

//    Route::group(['middleware' => ['admin']], function ()use($adminPrefix) {
    Route::get($adminPrefix . '/dashboard', ['as' => 'admin.dashboard', 'uses' => 'AdminController@dashboard']);
    Route::resource($adminPrefix . '/adminuser', 'Admin\AdminUserController');
    Route::resource($adminPrefix . '/users', 'Admin\UserController');
    Route::resource($adminPrefix . '/roles', 'Admin\RoleController');
    Route::any($adminPrefix . '/role-fetch', 'Admin\RoleController@FetchIndex')->name('role-fetch.index');
    Route::resource($adminPrefix . '/customers', 'Admin\UserCustomerController');
    Route::get($adminPrefix . '/customers-search', 'Admin\UserCustomerController@indexSearch');

    Route::resource($adminPrefix . '/suppliers', 'Admin\UserSupplierController');
    Route::get($adminPrefix . '/suppliers-search', 'Admin\UserSupplierController@indexSearch');
    Route::resource($adminPrefix . '/drivers', 'Admin\UserDriverController');
    Route::resource($adminPrefix . '/services', 'Admin\ServiceController');
    Route::resource($adminPrefix . '/governorate', 'Admin\LocationGovernorateController');
    Route::resource($adminPrefix . '/city', 'Admin\LocationCityController');
    Route::resource($adminPrefix . '/section', 'Admin\LocationSectionController');

    Route::resource($adminPrefix . '/admin-roles', 'Admin\AdminRoleController');
    Route::any($adminPrefix . '/admin-role-fetch', 'Admin\AdminRoleController@FetchIndex')->name('admin-role-fetch.index');


    Route::post($adminPrefix . '/import-csv-excel', array('as' => 'import-csv-excel', 'uses' => 'Admin\LocationGovernorateController@importFileIntoDB'));
    Route::post($adminPrefix . '/import-customer-csv-excel', array('as' => 'import-customer-csv-excel', 'uses' => 'Admin\UserController@customerimportFileIntoDB'));
    Route::post($adminPrefix . '/import-supplier-csv-excel', array('as' => 'import-supplier-csv-excel', 'uses' => 'Admin\UserController@supplierimportFileIntoDB'));
    Route::get($adminPrefix . '/configuration', 'Admin\ConfigurationController@index')->name('configuration.edit');
    Route::put($adminPrefix . '/configuration', 'Admin\ConfigurationController@update');
    Route::any($adminPrefix . '/location/governorate/changestate/{id}', 'Admin\LocationGovernorateController@changeState')->name('location.changestate');

    Route::any($adminPrefix . '/delete-state', 'Admin\LocationGovernorateController@multipledestroy')->name('multiple.delete');
    ;

    Route::any($adminPrefix . '/services/changestate/{id}', 'Admin\ServiceController@changeState')->name('services.changestate');
    Route::any($adminPrefix . '/user/changestate/{id}', 'Admin\UserController@changeState')->name('user.changestate');
//});
    Route::post($adminPrefix . '/notification/{type}/{target}', 'Admin\NotificationController@notify')->name('notify.send');
    Route::post('/logout', 'AdminLoginController@logout')->name('logout');
});


/* userlogin routes */

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('lang/{locale}', 'LocalizationController@index');
    Route::get('/login', 'UserLoginController@Login')->name('login.user');
    Route::get('/logout', 'UserLoginController@logout')->name('user.logout');
    Route::post('/login', 'UserLoginController@Auth')->name('login.user.auth');
    Route::get('/dashboard', 'HomeController@dashboard')->name('home.dashboard');
    Route::get('/services', 'HomeController@getservice')->name('home.service');
    Route::get('/profile', 'HomeController@profile')->name('home.profile');
    Route::get('/order-gas', 'HomeController@ordergas')->name('order.gas');
    Route::get('/order', 'HomeController@orderquantity')->name('home.order');
    Route::post('/save', 'HomeController@profilesave')->name('profile.save');
    Route::get('/number', 'HomeController@getphonenumber')->name('phone.no');
    Route::get('/order/{id}', 'HomeController@ordershow')->name('order.show');
});

   








