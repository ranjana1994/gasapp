<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminUsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $password = 'q1w2e3r4t5';

    public function run() {
//        try {

        $data = [
            'name' => 'admin',
            'email' => 'admin@gasapplication.com',
            'password' => $this->password,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'role' => 'super admin',
        ];
        $model = new App\AdminUser();
        foreach (array_keys(\App\Http\Controllers\Admin\AdminUserController::rules()) as $index):
            if ($index == 'role')
                continue;
            if ($index == 'password')
                $model->$index = Hash::make($data[$index]);
            else
                $model->$index = $data[$index];
        endforeach;
        $model->assignRole($data[$index]);
        $model->save();
//        } catch (\Exception $ex) {
//            echo 'dljlk';
//        }
    }

}
