<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSuppliersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('user_suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('vehicle_number')->unique();
            $table->string('license_number')->unique();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->integer('governorate_id')->unsigned()->index()->nullable();
            $table->foreign('governorate_id')->references('id')->on('locations')->onDelete('cascade');
            $table->integer('city_id')->unsigned()->index()->nullable();
            $table->foreign('city_id')->references('id')->on('locations')->onDelete('cascade');
            $table->integer('section_ids')->nullable();
            \App\Helpers\DbExtender::defaultParams($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('user_suppliers');
    }

}
