<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('last_name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('mobile_number')->unique();
            $table->string('mobile_number_tmp')->default(0);
            $table->string('otp')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->text('device_token')->nullable();
            $table->text('params')->nullable();
            $table->enum('status', [0, 1])->default(0)->comment('0->Unactive, 1->Active');
            $table->enum('state', [0, 1])->default(0)->comment('0->Unactive, 1->Active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}
