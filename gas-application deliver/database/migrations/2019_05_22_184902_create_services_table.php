<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('name')->nullable();
            $table->string('image_kr')->nullable();
            $table->string('name_kr')->nullable();
            $table->string('image_ar')->nullable();
            $table->string('name_ar')->nullable();
            $table->text('location')->nullable();
            $table->enum('unit', ['liter', 'quantity', 'set', 'barrel'])->nullable();
            $table->integer('parent_id')->default(0);
            \App\Helpers\DbExtender::defaultParams($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('services');
    }

}
